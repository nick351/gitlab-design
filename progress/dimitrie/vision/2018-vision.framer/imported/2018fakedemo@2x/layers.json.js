window.__imported__ = window.__imported__ || {};
window.__imported__["2018fakedemo@2x/layers.json.js"] = [
	{
		"objectId": "77E04AD3-4537-4965-AE66-C62E758C3A29",
		"kind": "artboard",
		"name": "view1_nav",
		"originalName": "view1_nav",
		"maskFrame": null,
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 1500,
			"height": 1024
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "084A5892-5143-44DC-AEEE-45B4D8237FBB",
				"kind": "group",
				"name": "topbar",
				"originalName": "topbar",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 1500,
					"height": 40
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-topbar-mdg0qtu4.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 1500,
						"height": 40
					}
				},
				"children": [
					{
						"objectId": "5D14C654-4FEB-4721-860D-BAF48BFACB10",
						"kind": "group",
						"name": "left_aligned",
						"originalName": "left-aligned",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 4,
							"width": 502,
							"height": 32
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-left_aligned-nuqxnem2.png",
							"frame": {
								"x": 16,
								"y": 4,
								"width": 502,
								"height": 32
							}
						},
						"children": []
					},
					{
						"objectId": "9EAE064E-60A7-48C0-B800-B847751BC877",
						"kind": "group",
						"name": "right_aligned",
						"originalName": "right-aligned",
						"maskFrame": null,
						"layerFrame": {
							"x": 1010,
							"y": 4,
							"width": 474,
							"height": 32
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-right_aligned-ouvbrta2.png",
							"frame": {
								"x": 1010,
								"y": 4,
								"width": 474,
								"height": 32
							}
						},
						"children": [
							{
								"objectId": "1E85F5AC-1938-4E2E-8EA4-3132623B1AAE",
								"kind": "group",
								"name": "user_items",
								"originalName": "user-items",
								"maskFrame": null,
								"layerFrame": {
									"x": 1298,
									"y": 8,
									"width": 186,
									"height": 24
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-user_items-muu4nuy1.png",
									"frame": {
										"x": 1298,
										"y": 8,
										"width": 186,
										"height": 24
									}
								},
								"children": []
							}
						]
					}
				]
			},
			{
				"objectId": "466A37DA-2B6F-47F1-8ECD-E41B4477B494",
				"kind": "group",
				"name": "right_sidebar",
				"originalName": "right-sidebar",
				"maskFrame": null,
				"layerFrame": {
					"x": 1210,
					"y": 40,
					"width": 290,
					"height": 984
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-right_sidebar-ndy2qtm3.png",
					"frame": {
						"x": 1210,
						"y": 40,
						"width": 290,
						"height": 984
					}
				},
				"children": [
					{
						"objectId": "B3319235-1FE2-4363-8A7D-94C00AFBAAC1",
						"kind": "group",
						"name": "top_aligned",
						"originalName": "top-aligned",
						"maskFrame": null,
						"layerFrame": {
							"x": 1227,
							"y": 56,
							"width": 257,
							"height": 648
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-top_aligned-qjmzmtky.png",
							"frame": {
								"x": 1227,
								"y": 56,
								"width": 257,
								"height": 648
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "4980ECBB-DC13-4763-9449-B89443B2B045",
				"kind": "group",
				"name": "left_sidebar",
				"originalName": "left-sidebar",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 40,
					"width": 220,
					"height": 984
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-left_sidebar-ndk4mevd.png",
					"frame": {
						"x": 0,
						"y": 40,
						"width": 220,
						"height": 984
					}
				},
				"children": [
					{
						"objectId": "1E01DD55-AF64-4ADC-A93E-700267E2AEE1",
						"kind": "group",
						"name": "top_aligned1",
						"originalName": "top-aligned",
						"maskFrame": null,
						"layerFrame": {
							"x": 0,
							"y": 48,
							"width": 220,
							"height": 417
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-top_aligned-muuwmure.png",
							"frame": {
								"x": 0,
								"y": 48,
								"width": 220,
								"height": 417
							}
						},
						"children": []
					},
					{
						"objectId": "E0EB8247-C4D3-4391-A387-402BCBB511BF",
						"kind": "group",
						"name": "bottom_aligned",
						"originalName": "bottom-aligned",
						"maskFrame": null,
						"layerFrame": {
							"x": 0,
							"y": 976,
							"width": 220,
							"height": 33
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-bottom_aligned-rtbfqjgy.png",
							"frame": {
								"x": 0,
								"y": 976,
								"width": 220,
								"height": 33
							}
						},
						"children": []
					}
				]
			}
		]
	},
	{
		"objectId": "6B84A0B4-0CB5-42E7-BB95-0315B8CDBF96",
		"kind": "artboard",
		"name": "view1_content",
		"originalName": "view1_content",
		"maskFrame": null,
		"layerFrame": {
			"x": 1600,
			"y": 0,
			"width": 990,
			"height": 2235
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "C242B4C4-8726-4AC7-AC1D-AB7C29BC0C5F",
				"kind": "group",
				"name": "below",
				"originalName": "below",
				"maskFrame": null,
				"layerFrame": {
					"x": 7,
					"y": 1757,
					"width": 982,
					"height": 473
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "5F9854D5-5325-4653-ACA8-80DAEC0EF5E4",
						"kind": "group",
						"name": "Group_11",
						"originalName": "Group 11",
						"maskFrame": null,
						"layerFrame": {
							"x": 7,
							"y": 1757,
							"width": 982,
							"height": 473
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_11-nuy5odu0.jpg",
							"frame": {
								"x": 7,
								"y": 1757,
								"width": 982,
								"height": 473
							}
						},
						"children": [
							{
								"objectId": "6DF5AFC2-416D-41D0-860F-2E3798F4B45A",
								"kind": "group",
								"name": "code_coverage_tooltip_1",
								"originalName": "code_coverage_tooltip_1",
								"maskFrame": null,
								"layerFrame": {
									"x": 68,
									"y": 2004,
									"width": 93,
									"height": 36
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-code_coverage_tooltip_1-nkrgnufg.png",
									"frame": {
										"x": 68,
										"y": 2004,
										"width": 93,
										"height": 36
									}
								},
								"children": []
							},
							{
								"objectId": "72E02F81-D5D3-46C3-906C-685DA02513A0",
								"kind": "group",
								"name": "code_coverage_1",
								"originalName": "code_coverage_1",
								"maskFrame": null,
								"layerFrame": {
									"x": 113,
									"y": 2042,
									"width": 4,
									"height": 135
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-code_coverage_1-nzjfmdjg.png",
									"frame": {
										"x": 113,
										"y": 2042,
										"width": 4,
										"height": 135
									}
								},
								"children": []
							},
							{
								"objectId": "66D78650-C95C-4DDA-B6DD-67DCAF60DB0A",
								"kind": "group",
								"name": "Group_5",
								"originalName": "Group 5",
								"maskFrame": null,
								"layerFrame": {
									"x": 117,
									"y": 2081,
									"width": 857,
									"height": 28
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_5-njzenzg2.png",
									"frame": {
										"x": 117,
										"y": 2081,
										"width": 857,
										"height": 28
									}
								},
								"children": []
							},
							{
								"objectId": "39FD370E-B11D-4EEF-AB5F-047F6F94991E",
								"kind": "group",
								"name": "issuable_row_emoji",
								"originalName": "issuable__row--emoji",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 1757,
									"width": 958,
									"height": 64
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-issuable_row_emoji-mzlgrdm3.png",
									"frame": {
										"x": 16,
										"y": 1757,
										"width": 958,
										"height": 64
									}
								},
								"children": [
									{
										"objectId": "330E7C5C-1384-4DDB-8B68-F7CCA7D4E811",
										"kind": "group",
										"name": "issue_actions",
										"originalName": "issue-actions",
										"maskFrame": null,
										"layerFrame": {
											"x": 782,
											"y": 1773,
											"width": 192,
											"height": 32
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-issue_actions-mzmwrtdd.png",
											"frame": {
												"x": 782,
												"y": 1773,
												"width": 192,
												"height": 32
											}
										},
										"children": []
									},
									{
										"objectId": "0CC621BD-5C0C-488D-A0A9-B40FF86064C3",
										"kind": "group",
										"name": "emoji_actions",
										"originalName": "emoji-actions",
										"maskFrame": null,
										"layerFrame": {
											"x": 16,
											"y": 1773,
											"width": 174,
											"height": 32
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-emoji_actions-mendnjix.png",
											"frame": {
												"x": 16,
												"y": 1773,
												"width": 174,
												"height": 32
											}
										},
										"children": []
									}
								]
							}
						]
					}
				]
			},
			{
				"objectId": "6B8B6817-93B3-4564-84F1-F6644F35AF6A",
				"kind": "group",
				"name": "row_merge",
				"originalName": "row_merge",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 1649,
					"width": 958,
					"height": 92
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-row_merge-nki4qjy4.png",
					"frame": {
						"x": 16,
						"y": 1649,
						"width": 958,
						"height": 92
					}
				},
				"children": [
					{
						"objectId": "9B4444BC-3D24-4CA9-9620-18F57F1EA15B",
						"kind": "group",
						"name": "secondary_row",
						"originalName": "secondary-row",
						"maskFrame": null,
						"layerFrame": {
							"x": 17,
							"y": 1705,
							"width": 956,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-secondary_row-oui0ndq0.png",
							"frame": {
								"x": 17,
								"y": 1705,
								"width": 956,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "37265054-4764-4AF1-A3C4-701F62A53770",
						"kind": "group",
						"name": "Column",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 819,
							"y": 1665,
							"width": 138,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-mzcynjuw.png",
							"frame": {
								"x": 819,
								"y": 1665,
								"width": 138,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "27EE7AA9-51B7-4248-9989-844A0CCF1CAF",
						"kind": "group",
						"name": "Column1",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 112,
							"y": 1670,
							"width": 193,
							"height": 16
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-mjdfrtdb.png",
							"frame": {
								"x": 112,
								"y": 1670,
								"width": 193,
								"height": 16
							}
						},
						"children": []
					},
					{
						"objectId": "5E488491-0249-4017-9555-A1CB1A7F5004",
						"kind": "group",
						"name": "Column2",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 32,
							"y": 1661,
							"width": 64,
							"height": 32
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-nuu0odg0.png",
							"frame": {
								"x": 32,
								"y": 1661,
								"width": 64,
								"height": 32
							}
						},
						"children": []
					},
					{
						"objectId": "621D9EA7-BB15-4F91-9CDE-60D774968DC7",
						"kind": "group",
						"name": "Column3",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 17,
							"y": 1649,
							"width": 56,
							"height": 56
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-njixrdlf.png",
							"frame": {
								"x": 17,
								"y": 1649,
								"width": 56,
								"height": 56
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "172FC7A5-B4A1-4518-8EE4-0381FCFDE214",
				"kind": "group",
				"name": "row_approve",
				"originalName": "row_approve",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 1593,
					"width": 958,
					"height": 57
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-row_approve-mtcyrkm3.png",
					"frame": {
						"x": 16,
						"y": 1593,
						"width": 958,
						"height": 57
					}
				},
				"children": [
					{
						"objectId": "D6B317ED-97CD-4DB2-94D6-F16D737E1E13",
						"kind": "group",
						"name": "Column4",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 637,
							"y": 1594,
							"width": 336,
							"height": 56
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-rdzcmze3.png",
							"frame": {
								"x": 637,
								"y": 1594,
								"width": 336,
								"height": 56
							}
						},
						"children": []
					},
					{
						"objectId": "CC34DEC2-9228-4389-8F58-D9D7420C99B7",
						"kind": "group",
						"name": "Column5",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 368,
							"y": 1594,
							"width": 269,
							"height": 56
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-q0mznerf.png",
							"frame": {
								"x": 368,
								"y": 1594,
								"width": 269,
								"height": 56
							}
						},
						"children": []
					},
					{
						"objectId": "655E3C7C-78B7-46B4-A801-59D04EAAC886",
						"kind": "group",
						"name": "Column6",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 71,
							"y": 1609,
							"width": 886,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-nju1rtnd.png",
							"frame": {
								"x": 71,
								"y": 1609,
								"width": 886,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "EBB5AA55-C3ED-43F0-826F-78F3F69B5669",
						"kind": "group",
						"name": "Column7",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 33,
							"y": 1610,
							"width": 24,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-rujcnufb.png",
							"frame": {
								"x": 33,
								"y": 1610,
								"width": 24,
								"height": 24
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "F821A188-92DE-4552-B405-4A1917AD47C8",
				"kind": "group",
				"name": "row_deploy",
				"originalName": "row_deploy",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 1538,
					"width": 958,
					"height": 56
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-row_deploy-rjgymuex.png",
					"frame": {
						"x": 16,
						"y": 1538,
						"width": 958,
						"height": 56
					}
				},
				"children": [
					{
						"objectId": "E2872B5E-A390-4F12-8094-21BA373B49EF",
						"kind": "group",
						"name": "Column8",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 829,
							"y": 1554,
							"width": 128,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-rti4nzjc.png",
							"frame": {
								"x": 829,
								"y": 1554,
								"width": 128,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "4D98F19F-A4B0-4187-949B-F7C43A15D56C",
						"kind": "group",
						"name": "Column9",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 365,
							"y": 1538,
							"width": 272,
							"height": 56
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-neq5oeyx.png",
							"frame": {
								"x": 365,
								"y": 1538,
								"width": 272,
								"height": 56
							}
						},
						"children": []
					},
					{
						"objectId": "698B9017-4EFC-4E32-BB39-B9DE3289FD43",
						"kind": "group",
						"name": "Column10",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 33,
							"y": 1554,
							"width": 552,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-njk4qjkw.png",
							"frame": {
								"x": 33,
								"y": 1554,
								"width": 552,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "94FF5C91-CDB1-441E-836D-FA4978BBC4D3",
						"kind": "group",
						"name": "Column11",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 33,
							"y": 1554,
							"width": 24,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-otrgrjvd.png",
							"frame": {
								"x": 33,
								"y": 1554,
								"width": 24,
								"height": 24
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "4C8373FD-CA4C-43BF-8737-3B75F2F40C76",
				"kind": "group",
				"name": "row_licensefinder",
				"originalName": "row_licensefinder",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 1483,
					"width": 958,
					"height": 56
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-row_licensefinder-nem4mzcz.png",
					"frame": {
						"x": 16,
						"y": 1483,
						"width": 958,
						"height": 56
					}
				},
				"children": [
					{
						"objectId": "FB661F77-7561-4AA9-B64C-0193B0EAFB50",
						"kind": "group",
						"name": "Column12",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 893,
							"y": 1498,
							"width": 64,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-rki2njfg.png",
							"frame": {
								"x": 893,
								"y": 1498,
								"width": 64,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "EF0A02F1-D7EE-4585-BAA7-A51265989CC6",
						"kind": "group",
						"name": "Column13",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 368,
							"y": 1483,
							"width": 269,
							"height": 56
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-ruywqtay.png",
							"frame": {
								"x": 368,
								"y": 1483,
								"width": 269,
								"height": 56
							}
						},
						"children": []
					},
					{
						"objectId": "BDC0D00A-A81E-4BD6-851E-3F3B3ABD0FD7",
						"kind": "group",
						"name": "Column14",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 39,
							"y": 1505,
							"width": 469,
							"height": 15
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-qkrdmeqw.png",
							"frame": {
								"x": 39,
								"y": 1505,
								"width": 469,
								"height": 15
							}
						},
						"children": [
							{
								"objectId": "D7BAFA73-CEEB-4FFA-BD54-6F3DAEDC7F26",
								"kind": "group",
								"name": "Group_3",
								"originalName": "Group 3",
								"maskFrame": null,
								"layerFrame": {
									"x": 39,
									"y": 1505,
									"width": 13,
									"height": 13
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_3-rddcquzb.png",
									"frame": {
										"x": 39,
										"y": 1505,
										"width": 13,
										"height": 13
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "63DE1CDF-74D6-44B4-9EF8-DD80C8724C17",
						"kind": "group",
						"name": "Column15",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 33,
							"y": 1499,
							"width": 24,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-njnertfd.png",
							"frame": {
								"x": 33,
								"y": 1499,
								"width": 24,
								"height": 24
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "DB6A56F8-A4EB-42BB-8FC4-F35124F6EBA6",
				"kind": "group",
				"name": "row_performancemetrics_expanded",
				"originalName": "row_performancemetrics_expanded",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 1410,
					"width": 958,
					"height": 74
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "2B25F2E4-E874-4125-8E8E-33140185A56A",
						"kind": "group",
						"name": "Group_9",
						"originalName": "Group 9",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 1410,
							"width": 958,
							"height": 74
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_9-mkiynuyy.png",
							"frame": {
								"x": 16,
								"y": 1410,
								"width": 958,
								"height": 74
							}
						},
						"children": [
							{
								"objectId": "1DDFAC9C-72B1-426A-8F67-DEF9DC6E5278",
								"kind": "group",
								"name": "Group_6",
								"originalName": "Group 6",
								"maskFrame": null,
								"layerFrame": {
									"x": 39,
									"y": 1430,
									"width": 256,
									"height": 38
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_6-murerkfd.png",
									"frame": {
										"x": 39,
										"y": 1430,
										"width": 256,
										"height": 38
									}
								},
								"children": []
							}
						]
					}
				]
			},
			{
				"objectId": "82A3BA9F-2BCB-4038-B47A-94A32BEE7B64",
				"kind": "group",
				"name": "row_performancemetrics",
				"originalName": "row_performancemetrics",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 1354,
					"width": 958,
					"height": 57
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-row_performancemetrics-odjbm0jb.png",
					"frame": {
						"x": 16,
						"y": 1354,
						"width": 958,
						"height": 57
					}
				},
				"children": [
					{
						"objectId": "FE9D9DC9-0141-47F7-8AD6-490779434378",
						"kind": "group",
						"name": "row_performancemetrics_button_expand",
						"originalName": "row_performancemetrics_button_expand",
						"maskFrame": null,
						"layerFrame": {
							"x": 893,
							"y": 1370,
							"width": 64,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-row_performancemetrics_button_expand-rku5rdle.png",
							"frame": {
								"x": 893,
								"y": 1370,
								"width": 64,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "1AAAB7E5-EF32-4A1F-A021-123C31824F47",
						"kind": "group",
						"name": "row_performancemetrics_button_collapse",
						"originalName": "row_performancemetrics_button_collapse",
						"maskFrame": null,
						"layerFrame": {
							"x": 886,
							"y": 1370,
							"width": 71,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-row_performancemetrics_button_collapse-mufbqui3.png",
							"frame": {
								"x": 886,
								"y": 1370,
								"width": 71,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "398EF19A-853C-4CF4-922D-20B556FF81F4",
						"kind": "group",
						"name": "Column16",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 368,
							"y": 1355,
							"width": 269,
							"height": 56
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-mzk4ruyx.png",
							"frame": {
								"x": 368,
								"y": 1355,
								"width": 269,
								"height": 56
							}
						},
						"children": []
					},
					{
						"objectId": "D895A74C-C030-4A71-A49F-FC11171F94E3",
						"kind": "group",
						"name": "Column17",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 74,
							"y": 1378,
							"width": 417,
							"height": 14
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-rdg5nue3.png",
							"frame": {
								"x": 74,
								"y": 1378,
								"width": 417,
								"height": 14
							}
						},
						"children": []
					},
					{
						"objectId": "3E5ABD94-F26A-416F-98F0-EDB70349F530",
						"kind": "group",
						"name": "Column18",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 33,
							"y": 1371,
							"width": 24,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-m0u1quje.png",
							"frame": {
								"x": 33,
								"y": 1371,
								"width": 24,
								"height": 24
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "4E8EFB8A-6157-46C8-A820-F4B94F23AE19",
				"kind": "group",
				"name": "row_securitytests_expanded",
				"originalName": "row_securitytests_expanded",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 919,
					"width": 958,
					"height": 436
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "F6DDE076-F985-434C-8A3F-069DE81064F5",
						"kind": "group",
						"name": "Group_7",
						"originalName": "Group 7",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 919,
							"width": 958,
							"height": 436
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_7-rjzereuw.png",
							"frame": {
								"x": 16,
								"y": 919,
								"width": 958,
								"height": 436
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "DC419073-976B-4AE1-A8ED-CC1CD1AE4BBF",
				"kind": "group",
				"name": "row_securitytests",
				"originalName": "row_securitytests",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 864,
					"width": 958,
					"height": 56
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-row_securitytests-rem0mtkw.png",
					"frame": {
						"x": 16,
						"y": 864,
						"width": 958,
						"height": 56
					}
				},
				"children": [
					{
						"objectId": "3B149A29-95AF-40A2-A7AC-496D14C22B66",
						"kind": "group",
						"name": "row_securitytests_button_expand",
						"originalName": "row_securitytests_button_expand",
						"maskFrame": null,
						"layerFrame": {
							"x": 893,
							"y": 879,
							"width": 64,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-row_securitytests_button_expand-m0ixndlb.png",
							"frame": {
								"x": 893,
								"y": 879,
								"width": 64,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "392E6425-B451-404D-85A0-85C275083B6D",
						"kind": "group",
						"name": "row_securitytests_button_collapse",
						"originalName": "row_securitytests_button_collapse",
						"maskFrame": null,
						"layerFrame": {
							"x": 886,
							"y": 879,
							"width": 71,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-row_securitytests_button_collapse-mzkyrty0.png",
							"frame": {
								"x": 886,
								"y": 879,
								"width": 71,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "4EBADB19-22D7-4208-94F7-6CAFDD9AA1FD",
						"kind": "group",
						"name": "Column19",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 368,
							"y": 864,
							"width": 269,
							"height": 56
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-nevcqurc.png",
							"frame": {
								"x": 368,
								"y": 864,
								"width": 269,
								"height": 56
							}
						},
						"children": []
					},
					{
						"objectId": "EA3D78E6-69DF-4DFD-9237-5D9E33DAC420",
						"kind": "group",
						"name": "Column20",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 72,
							"y": 864,
							"width": 296,
							"height": 56
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-ruezrdc4.png",
							"frame": {
								"x": 72,
								"y": 864,
								"width": 296,
								"height": 56
							}
						},
						"children": []
					},
					{
						"objectId": "7A510F70-B3FA-4024-BF70-3FE3470BB950",
						"kind": "text",
						"name": "Column21",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 33,
							"y": 880,
							"width": 24,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1,
							"string": "",
							"css": [
								"/* : */",
								"font-family: FontAwesome;",
								"font-size: 24px;",
								"color: #2E2E2E;",
								"line-height: 16px;"
							]
						},
						"image": {
							"path": "images/Layer-Column-n0e1mtbg.png",
							"frame": {
								"x": 33,
								"y": 880,
								"width": 24,
								"height": 24
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "51275DAC-595F-44CC-841A-CBDBCBFCE31A",
				"kind": "group",
				"name": "row_codequality_expanded",
				"originalName": "row_codequality_expanded",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 815,
					"width": 958,
					"height": 50
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "3C635AC0-7DA2-4A13-AD2A-A19ADFA4E70D",
						"kind": "group",
						"name": "Group_8",
						"originalName": "Group 8",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 815,
							"width": 958,
							"height": 50
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_8-m0m2mzvb.png",
							"frame": {
								"x": 16,
								"y": 815,
								"width": 958,
								"height": 50
							}
						},
						"children": [
							{
								"objectId": "22106785-17E4-4448-9DC0-6DC14182279D",
								"kind": "group",
								"name": "Group_2",
								"originalName": "Group 2",
								"maskFrame": null,
								"layerFrame": {
									"x": 41,
									"y": 835,
									"width": 365,
									"height": 14
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_2-mjixmdy3.png",
									"frame": {
										"x": 41,
										"y": 835,
										"width": 365,
										"height": 14
									}
								},
								"children": []
							}
						]
					}
				]
			},
			{
				"objectId": "50784F94-9EB1-430A-AEE4-5FDC633E4E97",
				"kind": "group",
				"name": "row_codequality",
				"originalName": "row_codequality",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 759,
					"width": 958,
					"height": 57
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-row_codequality-nta3odrg.png",
					"frame": {
						"x": 16,
						"y": 759,
						"width": 958,
						"height": 57
					}
				},
				"children": [
					{
						"objectId": "9C5F072F-B7B6-47BB-94CC-74FB9776FBD5",
						"kind": "group",
						"name": "row_codequality_button_expand",
						"originalName": "row_codequality_button_expand",
						"maskFrame": null,
						"layerFrame": {
							"x": 893,
							"y": 775,
							"width": 64,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-row_codequality_button_expand-oum1rja3.png",
							"frame": {
								"x": 893,
								"y": 775,
								"width": 64,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "2511561B-697B-4CBA-A149-26C771D93D25",
						"kind": "group",
						"name": "row_codequality_button_collapse",
						"originalName": "row_codequality_button_collapse",
						"maskFrame": null,
						"layerFrame": {
							"x": 886,
							"y": 775,
							"width": 71,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-row_codequality_button_collapse-mjuxmtu2.png",
							"frame": {
								"x": 886,
								"y": 775,
								"width": 71,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "F10851BF-B342-4ED2-AF57-FD682ADBCCAF",
						"kind": "group",
						"name": "Column22",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 369,
							"y": 760,
							"width": 269,
							"height": 56
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-rjewodux.png",
							"frame": {
								"x": 369,
								"y": 760,
								"width": 269,
								"height": 56
							}
						},
						"children": []
					},
					{
						"objectId": "BE0C5AB1-223C-41F9-9E85-9D016B92A3D4",
						"kind": "group",
						"name": "Column23",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 74,
							"y": 783,
							"width": 217,
							"height": 14
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-qkuwqzvb.png",
							"frame": {
								"x": 74,
								"y": 783,
								"width": 217,
								"height": 14
							}
						},
						"children": []
					},
					{
						"objectId": "D638CDC2-B52C-4FEE-85CD-82700F2BFB2C",
						"kind": "group",
						"name": "Column24",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 34,
							"y": 776,
							"width": 24,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-rdyzoene.png",
							"frame": {
								"x": 34,
								"y": 776,
								"width": 24,
								"height": 24
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "F720F431-4EA9-4533-9EB4-9C9B769BA211",
				"kind": "group",
				"name": "row_testsummary_expanded",
				"originalName": "row_testsummary_expanded",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 643,
					"width": 958,
					"height": 117
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "2D1955FB-7481-4091-9B14-AE238EBF7311",
						"kind": "group",
						"name": "Group_10",
						"originalName": "Group 10",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 643,
							"width": 958,
							"height": 117
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_10-mkqxotu1.png",
							"frame": {
								"x": 16,
								"y": 643,
								"width": 958,
								"height": 117
							}
						},
						"children": [
							{
								"objectId": "D87767EF-C994-4634-B594-5D43F0367768",
								"kind": "group",
								"name": "Group",
								"originalName": "Group",
								"maskFrame": null,
								"layerFrame": {
									"x": 40,
									"y": 659,
									"width": 917,
									"height": 84
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group-rdg3nzy3.png",
									"frame": {
										"x": 40,
										"y": 659,
										"width": 917,
										"height": 84
									}
								},
								"children": []
							}
						]
					}
				]
			},
			{
				"objectId": "100D354C-2ED7-4216-8377-D4D168127AC5",
				"kind": "group",
				"name": "row_testsummary",
				"originalName": "row_testsummary",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 587,
					"width": 958,
					"height": 56
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-row_testsummary-mtawrdm1.png",
					"frame": {
						"x": 16,
						"y": 587,
						"width": 958,
						"height": 56
					}
				},
				"children": [
					{
						"objectId": "175D8FAF-9798-4612-92CE-E7DAD26B6F2E",
						"kind": "group",
						"name": "row_testsummary_button_expand",
						"originalName": "row_testsummary_button_expand",
						"maskFrame": null,
						"layerFrame": {
							"x": 893,
							"y": 603,
							"width": 64,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-row_testsummary_button_expand-mtc1rdhg.png",
							"frame": {
								"x": 893,
								"y": 603,
								"width": 64,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "8C3DA6CF-26D0-4CED-AC37-6181B695041E",
						"kind": "group",
						"name": "row_testsummary_button_collapse",
						"originalName": "row_testsummary_button_collapse",
						"maskFrame": null,
						"layerFrame": {
							"x": 886,
							"y": 603,
							"width": 71,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-row_testsummary_button_collapse-oemzree2.png",
							"frame": {
								"x": 886,
								"y": 603,
								"width": 71,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "45B4427B-5D02-412E-B7D0-15225C1A87AA",
						"kind": "group",
						"name": "Column25",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 270,
							"y": 587,
							"width": 272,
							"height": 56
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-ndvcndqy.png",
							"frame": {
								"x": 270,
								"y": 587,
								"width": 272,
								"height": 56
							}
						},
						"children": []
					},
					{
						"objectId": "0958BFD8-F1F9-4B73-B6CE-A9BC839E8EF2",
						"kind": "group",
						"name": "Column26",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 72,
							"y": 610,
							"width": 242,
							"height": 14
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-mdk1oejg.png",
							"frame": {
								"x": 72,
								"y": 610,
								"width": 242,
								"height": 14
							}
						},
						"children": []
					},
					{
						"objectId": "BE42C5CD-AFA8-4581-8BAF-EED49DAF5110",
						"kind": "group",
						"name": "Column27",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 33,
							"y": 603,
							"width": 24,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-qku0mkm1.png",
							"frame": {
								"x": 33,
								"y": 603,
								"width": 24,
								"height": 24
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "F49DF384-3CD1-472F-BFCF-D03E05E4FF7A",
				"kind": "group",
				"name": "row_pipeline",
				"originalName": "row_pipeline",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 532,
					"width": 958,
					"height": 56
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-row_pipeline-rjq5reyz.png",
					"frame": {
						"x": 16,
						"y": 532,
						"width": 958,
						"height": 56
					}
				},
				"children": [
					{
						"objectId": "9FA05113-038C-4B29-99F4-CD9EE9FCB79D",
						"kind": "group",
						"name": "row_pipeline_button_expand",
						"originalName": "row_pipeline_button_expand",
						"maskFrame": null,
						"layerFrame": {
							"x": 893,
							"y": 548,
							"width": 64,
							"height": 24
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-row_pipeline_button_expand-ouzbmdux.png",
							"frame": {
								"x": 893,
								"y": 548,
								"width": 64,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "8C2EFFE4-500E-4CF2-8934-0EBAE18CF1A2",
						"kind": "group",
						"name": "row_pipeline_button_artifacts",
						"originalName": "row_pipeline_button_artifacts",
						"maskFrame": null,
						"layerFrame": {
							"x": 816,
							"y": 548,
							"width": 141,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-row_pipeline_button_artifacts-oemyruzg.png",
							"frame": {
								"x": 816,
								"y": 548,
								"width": 141,
								"height": 24
							}
						},
						"children": [
							{
								"objectId": "594377E5-A5CC-44E9-BA71-87E3C63D6525",
								"kind": "group",
								"name": "caret",
								"originalName": "caret",
								"maskFrame": {
									"x": 0.2410714285714284,
									"y": 0,
									"width": 3.374999999999997,
									"height": 8
								},
								"layerFrame": {
									"x": 940.0000000000001,
									"y": 559,
									"width": 9,
									"height": 4
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-caret-ntk0mzc3.png",
									"frame": {
										"x": 940.0000000000001,
										"y": 559,
										"width": 9,
										"height": 4
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "750A32C8-9FED-4AC5-89C0-93D728C84138",
						"kind": "group",
						"name": "row_pipeline_button_artifacts1",
						"originalName": "row_pipeline_button_artifacts",
						"maskFrame": null,
						"layerFrame": {
							"x": 726,
							"y": 548,
							"width": 82,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-row_pipeline_button_artifacts-nzuwqtmy.png",
							"frame": {
								"x": 726,
								"y": 548,
								"width": 82,
								"height": 24
							}
						},
						"children": [
							{
								"objectId": "7BA7D42D-9A85-4EEA-B733-DC49208794F3",
								"kind": "group",
								"name": "caret1",
								"originalName": "caret",
								"maskFrame": {
									"x": 0.2410714285714284,
									"y": 0,
									"width": 3.374999999999997,
									"height": 8
								},
								"layerFrame": {
									"x": 791.0000000000001,
									"y": 559,
									"width": 9,
									"height": 4
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-caret-n0jbn0q0.png",
									"frame": {
										"x": 791.0000000000001,
										"y": 559,
										"width": 9,
										"height": 4
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "247345DB-5C8B-436B-B61C-657F084EFBD6",
						"kind": "group",
						"name": "row_pipeline_button_collapse",
						"originalName": "row_pipeline_button_collapse",
						"maskFrame": null,
						"layerFrame": {
							"x": 886,
							"y": 548,
							"width": 71,
							"height": 24
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-row_pipeline_button_collapse-mjq3mzq1.png",
							"frame": {
								"x": 886,
								"y": 548,
								"width": 71,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "0733151F-3CE0-4DC0-8B80-FEC9502F76CC",
						"kind": "group",
						"name": "Column28",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 249,
							"y": 549,
							"width": 126,
							"height": 22
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-mdczmze1.png",
							"frame": {
								"x": 249,
								"y": 549,
								"width": 126,
								"height": 22
							}
						},
						"children": [
							{
								"objectId": "3F772C8A-7487-47E0-BBF3-8F4BDE331125",
								"kind": "group",
								"name": "pipelines_minipipeline_graph",
								"originalName": "pipelines/minipipeline-graph",
								"maskFrame": null,
								"layerFrame": {
									"x": 249,
									"y": 549,
									"width": 126,
									"height": 22
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-pipelines_minipipeline_graph-m0y3nzjd.png",
									"frame": {
										"x": 249,
										"y": 549,
										"width": 126,
										"height": 22
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "7B66897C-DC33-477A-AD8B-0FB1AA6B0635",
						"kind": "group",
						"name": "Column29",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 73,
							"y": 555,
							"width": 422,
							"height": 14
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-n0i2njg5.png",
							"frame": {
								"x": 73,
								"y": 555,
								"width": 422,
								"height": 14
							}
						},
						"children": []
					},
					{
						"objectId": "19EC0191-3AC8-43F9-83B1-81A5C733838C",
						"kind": "group",
						"name": "Column30",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 33,
							"y": 548,
							"width": 24,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-mtlfqzax.png",
							"frame": {
								"x": 33,
								"y": 548,
								"width": 24,
								"height": 24
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "6C44E1BB-102F-4E59-BB14-8585E609EE25",
				"kind": "group",
				"name": "header",
				"originalName": "header",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 483,
					"width": 958,
					"height": 50
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-header-nkm0neux.png",
					"frame": {
						"x": 16,
						"y": 483,
						"width": 958,
						"height": 50
					}
				},
				"children": []
			},
			{
				"objectId": "94B8248B-6677-470A-BE37-E3D49F5ADE71",
				"kind": "group",
				"name": "top",
				"originalName": "top",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 0,
					"width": 958,
					"height": 467
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "F4730364-78F1-403A-8A32-B35E5FB18178",
						"kind": "group",
						"name": "breadcrumbs",
						"originalName": "breadcrumbs",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 0,
							"width": 958,
							"height": 48
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-breadcrumbs-rjq3mzaz.png",
							"frame": {
								"x": 16,
								"y": 0,
								"width": 958,
								"height": 48
							}
						},
						"children": [
							{
								"objectId": "A437591C-DFA3-4561-81D1-74690B049971",
								"kind": "group",
								"name": "breadcrumb_items",
								"originalName": "breadcrumb-items",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 16,
									"width": 412,
									"height": 16
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-breadcrumb_items-qtqznzu5.png",
									"frame": {
										"x": 16,
										"y": 16,
										"width": 412,
										"height": 16
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "0AB54A52-B0F5-47AB-B61B-FD46D1506747",
						"kind": "group",
						"name": "issuable_row_header",
						"originalName": "issuable__row--header",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 63,
							"width": 958,
							"height": 48
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-issuable_row_header-mefcntrb.png",
							"frame": {
								"x": 16,
								"y": 63,
								"width": 958,
								"height": 48
							}
						},
						"children": [
							{
								"objectId": "A985B96F-4767-4C78-8FAE-8EC7FCF6EDAB",
								"kind": "group",
								"name": "meta_data",
								"originalName": "meta-data",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 67,
									"width": 618,
									"height": 24
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-meta_data-qtk4nui5.png",
									"frame": {
										"x": 16,
										"y": 67,
										"width": 618,
										"height": 24
									}
								},
								"children": [
									{
										"objectId": "337B4BB5-63E3-4056-8F70-8E605F70FB08",
										"kind": "group",
										"name": "text",
										"originalName": "text",
										"maskFrame": null,
										"layerFrame": {
											"x": 76,
											"y": 67,
											"width": 558,
											"height": 24
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-text-mzm3qjrc.png",
											"frame": {
												"x": 76,
												"y": 67,
												"width": 558,
												"height": 24
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "4741A4EA-916B-495B-B87D-68C0E2670AEA",
								"kind": "group",
								"name": "actions",
								"originalName": "actions",
								"maskFrame": null,
								"layerFrame": {
									"x": 788,
									"y": 63,
									"width": 186,
									"height": 32
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-actions-ndc0mue0.png",
									"frame": {
										"x": 788,
										"y": 63,
										"width": 186,
										"height": 32
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "E1855E3E-8CCD-4AC2-A8BB-6D0A926C62A5",
						"kind": "group",
						"name": "issuable_description",
						"originalName": "issuable__description",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 127,
							"width": 958,
							"height": 340
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-issuable_description-rte4ntvf.png",
							"frame": {
								"x": 16,
								"y": 127,
								"width": 958,
								"height": 340
							}
						},
						"children": []
					}
				]
			}
		]
	},
	{
		"objectId": "E5713E49-8169-4FBD-8D65-EC9B92167213",
		"kind": "artboard",
		"name": "view2_nav",
		"originalName": "view2_nav",
		"maskFrame": null,
		"layerFrame": {
			"x": 2690,
			"y": 0,
			"width": 1500,
			"height": 1024
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "EB9ABE38-5AB2-41ED-AEB3-6D40D77ECB39",
				"kind": "group",
				"name": "topbar1",
				"originalName": "topbar",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 1500,
					"height": 40
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-topbar-rui5qujf.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 1500,
						"height": 40
					}
				},
				"children": [
					{
						"objectId": "2C21D0DC-2D69-4DE7-8D5E-4CDA4C08920F",
						"kind": "group",
						"name": "left_aligned1",
						"originalName": "left-aligned",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 4,
							"width": 502,
							"height": 32
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-left_aligned-mkmymuqw.png",
							"frame": {
								"x": 16,
								"y": 4,
								"width": 502,
								"height": 32
							}
						},
						"children": []
					},
					{
						"objectId": "E58AB52D-99A2-44FC-AA92-BD3DFA7D5108",
						"kind": "group",
						"name": "right_aligned1",
						"originalName": "right-aligned",
						"maskFrame": null,
						"layerFrame": {
							"x": 981,
							"y": 4,
							"width": 503,
							"height": 32
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-right_aligned-rtu4qui1.png",
							"frame": {
								"x": 981,
								"y": 4,
								"width": 503,
								"height": 32
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "C2CC34A2-0C2A-414C-9B94-9100C03002B3",
				"kind": "group",
				"name": "left_sidebar1",
				"originalName": "left-sidebar",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 40,
					"width": 222,
					"height": 984
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-left_sidebar-qzjdqzm0.png",
					"frame": {
						"x": 0,
						"y": 40,
						"width": 222,
						"height": 984
					}
				},
				"children": [
					{
						"objectId": "9A29042C-73D4-40D5-A79C-EEB6611131DB",
						"kind": "group",
						"name": "top_aligned2",
						"originalName": "top-aligned",
						"maskFrame": null,
						"layerFrame": {
							"x": 0,
							"y": 52,
							"width": 220,
							"height": 409
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-top_aligned-oueyota0.png",
							"frame": {
								"x": 0,
								"y": 52,
								"width": 220,
								"height": 409
							}
						},
						"children": [
							{
								"objectId": "C1F26C6B-A726-420C-AB0B-65E50AAD5E8D",
								"kind": "group",
								"name": "sidebar_molecules_header",
								"originalName": "sidebar/molecules/header",
								"maskFrame": null,
								"layerFrame": {
									"x": 8,
									"y": 52,
									"width": 118,
									"height": 40
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-sidebar_molecules_header-qzfgmjzd.png",
									"frame": {
										"x": 8,
										"y": 52,
										"width": 118,
										"height": 40
									}
								},
								"children": [
									{
										"objectId": "F2178893-C21F-4C13-BE46-2F0925DAB6CB",
										"kind": "group",
										"name": "project_icon",
										"originalName": "project-icon",
										"maskFrame": null,
										"layerFrame": {
											"x": 8,
											"y": 52,
											"width": 40,
											"height": 40
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-project_icon-rjixnzg4.png",
											"frame": {
												"x": 8,
												"y": 52,
												"width": 40,
												"height": 40
											}
										},
										"children": [
											{
												"objectId": "7E8F5C9B-01C9-42E0-B50E-B8D84AD4D8C3",
												"kind": "group",
												"name": "gitlab_logo",
												"originalName": "gitlab-logo",
												"maskFrame": null,
												"layerFrame": {
													"x": 15.000000000000004,
													"y": 60.00000000000001,
													"width": 26,
													"height": 24
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-gitlab_logo-n0u4rjvd.png",
													"frame": {
														"x": 15.000000000000004,
														"y": 60.00000000000001,
														"width": 26,
														"height": 24
													}
												},
												"children": []
											}
										]
									}
								]
							}
						]
					},
					{
						"objectId": "90C4005F-1F51-46F7-BBDC-6336DE427D26",
						"kind": "group",
						"name": "bottom_aligned1",
						"originalName": "bottom-aligned",
						"maskFrame": null,
						"layerFrame": {
							"x": 0,
							"y": 984,
							"width": 220,
							"height": 29
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-bottom_aligned-otbdndaw.png",
							"frame": {
								"x": 0,
								"y": 984,
								"width": 220,
								"height": 29
							}
						},
						"children": []
					}
				]
			}
		]
	},
	{
		"objectId": "82929E3F-3C54-411D-890D-85C4AA8DF022",
		"kind": "artboard",
		"name": "view2_content",
		"originalName": "view2_content",
		"maskFrame": null,
		"layerFrame": {
			"x": 4290,
			"y": 0,
			"width": 1280,
			"height": 1024
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "507C570F-4FC5-4F1F-B699-416051109794",
				"kind": "group",
				"name": "Group_13",
				"originalName": "Group 13",
				"maskFrame": null,
				"layerFrame": {
					"x": 15,
					"y": 204,
					"width": 1249,
					"height": 566
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "1373549D-4D29-499F-9821-18C6A8DF4C4C",
						"kind": "group",
						"name": "Group_16",
						"originalName": "Group 16",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 640,
							"width": 1246,
							"height": 129
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_16-mtm3mzu0.png",
							"frame": {
								"x": 16,
								"y": 640,
								"width": 1246,
								"height": 129
							}
						},
						"children": [
							{
								"objectId": "F609E126-91F8-4EE8-AE5C-B1CDB9B6A530",
								"kind": "group",
								"name": "Group_15",
								"originalName": "Group 15",
								"maskFrame": null,
								"layerFrame": {
									"x": 202,
									"y": 738,
									"width": 934,
									"height": 15
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_15-rjywouux.png",
									"frame": {
										"x": 202,
										"y": 738,
										"width": 934,
										"height": 15
									}
								},
								"children": []
							},
							{
								"objectId": "F7C935E1-B116-4F0D-8A5A-F4050282B4B7",
								"kind": "group",
								"name": "Group_151",
								"originalName": "Group 15",
								"maskFrame": null,
								"layerFrame": {
									"x": 202,
									"y": 718,
									"width": 970,
									"height": 15
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_15-rjddotm1.png",
									"frame": {
										"x": 202,
										"y": 718,
										"width": 970,
										"height": 15
									}
								},
								"children": []
							},
							{
								"objectId": "A0F4C1E7-2805-4D8A-8BCB-7BC26B52150F",
								"kind": "group",
								"name": "global_components_buttons_count",
								"originalName": "global-components/buttons/count",
								"maskFrame": null,
								"layerFrame": {
									"x": 181,
									"y": 737,
									"width": 62,
									"height": 16
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-global_components_buttons_count-qtbgnemx.png",
									"frame": {
										"x": 181,
										"y": 737,
										"width": 62,
										"height": 16
									}
								},
								"children": [
									{
										"objectId": "50A84A93-719D-4965-AC1F-C6A4529EEC9C",
										"kind": "group",
										"name": "global_components_buttons_default_small",
										"originalName": "global-components/buttons/default/small",
										"maskFrame": null,
										"layerFrame": {
											"x": 196,
											"y": 737,
											"width": 47,
											"height": 16
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-global_components_buttons_default_small-ntbbodrb.png",
											"frame": {
												"x": 196,
												"y": 737,
												"width": 47,
												"height": 16
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "C54EACAE-5B68-426A-9041-7F0F2D7208AD",
								"kind": "group",
								"name": "global_components_buttons_count1",
								"originalName": "global-components/buttons/count",
								"maskFrame": null,
								"layerFrame": {
									"x": 181,
									"y": 717,
									"width": 62,
									"height": 16
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-global_components_buttons_count-qzu0rufd.png",
									"frame": {
										"x": 181,
										"y": 717,
										"width": 62,
										"height": 16
									}
								},
								"children": [
									{
										"objectId": "6014A037-EE91-4D3B-B12F-BDEDDB85C088",
										"kind": "group",
										"name": "global_components_buttons_default_small1",
										"originalName": "global-components/buttons/default/small",
										"maskFrame": null,
										"layerFrame": {
											"x": 196,
											"y": 717,
											"width": 47,
											"height": 16
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-global_components_buttons_default_small-njaxneew.png",
											"frame": {
												"x": 196,
												"y": 717,
												"width": 47,
												"height": 16
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "A0F04A35-C788-4FBE-A0FD-EE3DA47DB39D",
								"kind": "group",
								"name": "global_components_buttons_count2",
								"originalName": "global-components/buttons/count",
								"maskFrame": null,
								"layerFrame": {
									"x": 181,
									"y": 697,
									"width": 62,
									"height": 16
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-global_components_buttons_count-qtbgmdrb.png",
									"frame": {
										"x": 181,
										"y": 697,
										"width": 62,
										"height": 16
									}
								},
								"children": [
									{
										"objectId": "FD010A49-0B12-4C65-8BE5-4DC6E9B68DF7",
										"kind": "group",
										"name": "global_components_buttons_default_small2",
										"originalName": "global-components/buttons/default/small",
										"maskFrame": null,
										"layerFrame": {
											"x": 196,
											"y": 697,
											"width": 47,
											"height": 16
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-global_components_buttons_default_small-rkqwmtbb.png",
											"frame": {
												"x": 196,
												"y": 697,
												"width": 47,
												"height": 16
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "D5BF0557-6E95-403C-BC7E-B78A904E13C8",
								"kind": "group",
								"name": "global_components_buttons_count3",
								"originalName": "global-components/buttons/count",
								"maskFrame": null,
								"layerFrame": {
									"x": 181,
									"y": 677,
									"width": 62,
									"height": 16
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-global_components_buttons_count-rdvcrja1.png",
									"frame": {
										"x": 181,
										"y": 677,
										"width": 62,
										"height": 16
									}
								},
								"children": [
									{
										"objectId": "D2E7FA31-F669-4636-8FBE-E40FD7C4D3A6",
										"kind": "group",
										"name": "global_components_buttons_default_small3",
										"originalName": "global-components/buttons/default/small",
										"maskFrame": null,
										"layerFrame": {
											"x": 196,
											"y": 677,
											"width": 47,
											"height": 16
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-global_components_buttons_default_small-rdjfn0zb.png",
											"frame": {
												"x": 196,
												"y": 677,
												"width": 47,
												"height": 16
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "7F84AB7A-AF57-483C-8E53-E7555B9FAE3E",
								"kind": "group",
								"name": "Group_51",
								"originalName": "Group 5",
								"maskFrame": null,
								"layerFrame": {
									"x": 202,
									"y": 698,
									"width": 1006,
									"height": 15
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_5-n0y4nefc.png",
									"frame": {
										"x": 202,
										"y": 698,
										"width": 1006,
										"height": 15
									}
								},
								"children": []
							},
							{
								"objectId": "3DADE2B2-CC5C-493E-8C6E-3DA20048ECEF",
								"kind": "group",
								"name": "Group_52",
								"originalName": "Group 5",
								"maskFrame": null,
								"layerFrame": {
									"x": 202,
									"y": 678,
									"width": 952,
									"height": 15
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_5-m0rbreuy.png",
									"frame": {
										"x": 202,
										"y": 678,
										"width": 952,
										"height": 15
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "0B18C3B8-8FBF-419B-BABD-5385FD10D180",
						"kind": "group",
						"name": "Env_Stage_Copy",
						"originalName": "Env_Stage Copy",
						"maskFrame": null,
						"layerFrame": {
							"x": 15,
							"y": 204,
							"width": 1249,
							"height": 566
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Env_Stage_Copy-meixoemz.png",
							"frame": {
								"x": 15,
								"y": 204,
								"width": 1249,
								"height": 566
							}
						},
						"children": [
							{
								"objectId": "079AAF21-D206-4AB7-84E1-F8C4AAAA867E",
								"kind": "group",
								"name": "memory",
								"originalName": "memory",
								"maskFrame": null,
								"layerFrame": {
									"x": 653,
									"y": 285,
									"width": 598,
									"height": 211
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-memory-mdc5qufg.png",
									"frame": {
										"x": 653,
										"y": 285,
										"width": 598,
										"height": 211
									}
								},
								"children": [
									{
										"objectId": "7D38C8C3-CF28-4507-BC08-D1F5EF8E05D3",
										"kind": "group",
										"name": "Group_31",
										"originalName": "Group 3",
										"maskFrame": null,
										"layerFrame": {
											"x": 653,
											"y": 321,
											"width": 594,
											"height": 175
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_3-n0qzoem4.png",
											"frame": {
												"x": 653,
												"y": 321,
												"width": 594,
												"height": 175
											}
										},
										"children": [
											{
												"objectId": "D63736CE-3FDF-4D1D-A903-3CD173994C45",
												"kind": "group",
												"name": "Group_53",
												"originalName": "Group 5",
												"maskFrame": null,
												"layerFrame": {
													"x": 943,
													"y": 480,
													"width": 38,
													"height": 16
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_5-rdyznzm2.png",
													"frame": {
														"x": 943,
														"y": 480,
														"width": 38,
														"height": 16
													}
												},
												"children": []
											},
											{
												"objectId": "F97F96BF-B5BB-4A62-B983-B8FD53A99C81",
												"kind": "group",
												"name": "Group_4",
												"originalName": "Group 4",
												"maskFrame": null,
												"layerFrame": {
													"x": 653,
													"y": 331,
													"width": 16,
													"height": 72
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_4-rjk3rjk2.png",
													"frame": {
														"x": 653,
														"y": 331,
														"width": 16,
														"height": 72
													}
												},
												"children": []
											},
											{
												"objectId": "145041F5-918E-4A04-AE01-B413889C2D4C",
												"kind": "group",
												"name": "y_axis",
												"originalName": "y-axis",
												"maskFrame": null,
												"layerFrame": {
													"x": 674,
													"y": 321,
													"width": 45,
													"height": 137
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-y_axis-mtq1mdqx.png",
													"frame": {
														"x": 674,
														"y": 321,
														"width": 45,
														"height": 137
													}
												},
												"children": [
													{
														"objectId": "489D334B-595E-4554-9787-EECC9ED6D273",
														"kind": "group",
														"name": "y_axis_tick",
														"originalName": "y-axis__tick",
														"maskFrame": null,
														"layerFrame": {
															"x": 674,
															"y": 321,
															"width": 44,
															"height": 11
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-y_axis_tick-ndg5rdmz.png",
															"frame": {
																"x": 674,
																"y": 321,
																"width": 44,
																"height": 11
															}
														},
														"children": []
													},
													{
														"objectId": "AF36F2A6-24F3-40CE-B028-D7FBC2BDF928",
														"kind": "group",
														"name": "y_axis_tick1",
														"originalName": "y-axis__tick",
														"maskFrame": null,
														"layerFrame": {
															"x": 703,
															"y": 448,
															"width": 15,
															"height": 10
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-y_axis_tick-quyznkyy.png",
															"frame": {
																"x": 703,
																"y": 448,
																"width": 15,
																"height": 10
															}
														},
														"children": []
													}
												]
											},
											{
												"objectId": "290BFED8-7A2D-497C-B832-37DB47BF21E8",
												"kind": "group",
												"name": "x_axis",
												"originalName": "x-axis",
												"maskFrame": null,
												"layerFrame": {
													"x": 719,
													"y": 452,
													"width": 528,
													"height": 21
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-x_axis-mjkwqkzf.png",
													"frame": {
														"x": 719,
														"y": 452,
														"width": 528,
														"height": 21
													}
												},
												"children": []
											}
										]
									}
								]
							},
							{
								"objectId": "91133FCB-82D9-45D7-BC06-761A5DFBCB02",
								"kind": "group",
								"name": "cpu",
								"originalName": "cpu",
								"maskFrame": null,
								"layerFrame": {
									"x": 31,
									"y": 284,
									"width": 827,
									"height": 238
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "578F9849-ED51-4802-B2A6-BB6D1AFADD9E",
										"kind": "group",
										"name": "cpu1",
										"originalName": "cpu",
										"maskFrame": null,
										"layerFrame": {
											"x": 31,
											"y": 284,
											"width": 827,
											"height": 238
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-cpu-ntc4rjk4.png",
											"frame": {
												"x": 31,
												"y": 284,
												"width": 827,
												"height": 238
											}
										},
										"children": [
											{
												"objectId": "22607FB2-43C6-4277-9A1D-F51AAFE90F04",
												"kind": "group",
												"name": "Group_21",
												"originalName": "Group 2",
												"maskFrame": null,
												"layerFrame": {
													"x": 566,
													"y": 317,
													"width": 59,
													"height": 30
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_2-mji2mddg.png",
													"frame": {
														"x": 566,
														"y": 317,
														"width": 59,
														"height": 30
													}
												},
												"children": []
											},
											{
												"objectId": "C0CA8554-2541-4CCF-81CD-84AD0D12DC76",
												"kind": "group",
												"name": "Group_32",
												"originalName": "Group 3",
												"maskFrame": null,
												"layerFrame": {
													"x": 31,
													"y": 322,
													"width": 594,
													"height": 174
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_3-qzbdqtg1.png",
													"frame": {
														"x": 31,
														"y": 322,
														"width": 594,
														"height": 174
													}
												},
												"children": [
													{
														"objectId": "F54DCA10-7E19-4681-B6CB-477559A92BDF",
														"kind": "group",
														"name": "Group_54",
														"originalName": "Group 5",
														"maskFrame": null,
														"layerFrame": {
															"x": 321,
															"y": 480,
															"width": 38,
															"height": 16
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_5-rju0renb.png",
															"frame": {
																"x": 321,
																"y": 480,
																"width": 38,
																"height": 16
															}
														},
														"children": []
													},
													{
														"objectId": "A2780D82-98BA-4951-B2AE-1A521671269C",
														"kind": "group",
														"name": "Group_41",
														"originalName": "Group 4",
														"maskFrame": null,
														"layerFrame": {
															"x": 31,
															"y": 342,
															"width": 16,
															"height": 58
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_4-qti3odbe.png",
															"frame": {
																"x": 31,
																"y": 342,
																"width": 16,
																"height": 58
															}
														},
														"children": []
													},
													{
														"objectId": "C363249C-247D-4922-B1CB-42549B57EF26",
														"kind": "group",
														"name": "y_axis1",
														"originalName": "y-axis",
														"maskFrame": null,
														"layerFrame": {
															"x": 59,
															"y": 322,
															"width": 38,
															"height": 136
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-y_axis-qzm2mzi0.png",
															"frame": {
																"x": 59,
																"y": 322,
																"width": 38,
																"height": 136
															}
														},
														"children": [
															{
																"objectId": "91824BB4-1430-4001-B996-8ABEB70BA59A",
																"kind": "group",
																"name": "y_axis_tick2",
																"originalName": "y-axis__tick",
																"maskFrame": null,
																"layerFrame": {
																	"x": 59,
																	"y": 322,
																	"width": 37,
																	"height": 10
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-y_axis_tick-ote4mjrc.png",
																	"frame": {
																		"x": 59,
																		"y": 322,
																		"width": 37,
																		"height": 10
																	}
																},
																"children": []
															},
															{
																"objectId": "3D5621FD-CAC2-46CD-873C-D1502D6ED50C",
																"kind": "group",
																"name": "y_axis_tick3",
																"originalName": "y-axis__tick",
																"maskFrame": null,
																"layerFrame": {
																	"x": 81,
																	"y": 448,
																	"width": 15,
																	"height": 10
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-y_axis_tick-m0q1njix.png",
																	"frame": {
																		"x": 81,
																		"y": 448,
																		"width": 15,
																		"height": 10
																	}
																},
																"children": []
															}
														]
													},
													{
														"objectId": "F05597D0-D1DC-401E-AF4B-E86274F20DA2",
														"kind": "group",
														"name": "x_axis1",
														"originalName": "x-axis",
														"maskFrame": null,
														"layerFrame": {
															"x": 97,
															"y": 452,
															"width": 528,
															"height": 21
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-x_axis-rja1ntk3.png",
															"frame": {
																"x": 97,
																"y": 452,
																"width": 528,
																"height": 21
															}
														},
														"children": []
													}
												]
											}
										]
									}
								]
							}
						]
					}
				]
			},
			{
				"objectId": "9700DEB3-CBAE-4721-BD82-CA6B3E26B0F9",
				"kind": "group",
				"name": "Group_131",
				"originalName": "Group 13",
				"maskFrame": null,
				"layerFrame": {
					"x": 15,
					"y": 64,
					"width": 1249,
					"height": 124
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "826BD63F-5892-4462-924E-EF02A68408B6",
						"kind": "group",
						"name": "Env_Stage_Copy1",
						"originalName": "Env_Stage Copy",
						"maskFrame": null,
						"layerFrame": {
							"x": 15,
							"y": 64,
							"width": 1249,
							"height": 124
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Env_Stage_Copy-odi2qkq2.png",
							"frame": {
								"x": 15,
								"y": 64,
								"width": 1249,
								"height": 124
							}
						},
						"children": [
							{
								"objectId": "A0FF1543-1533-4F21-B11F-3118393AC7E9",
								"kind": "group",
								"name": "Group_22",
								"originalName": "Group 2",
								"maskFrame": {
									"x": 0,
									"y": 0,
									"width": 290.9999999999978,
									"height": 45.00000000000001
								},
								"layerFrame": {
									"x": 956,
									"y": 126,
									"width": 291,
									"height": 45
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_2-qtbgrje1.png",
									"frame": {
										"x": 956,
										"y": 126,
										"width": 291,
										"height": 45
									}
								},
								"children": [
									{
										"objectId": "8E68BF77-E594-4761-9A3A-7EAE080AB1E7",
										"kind": "group",
										"name": "Group_3_Copy",
										"originalName": "Group 3 Copy",
										"maskFrame": null,
										"layerFrame": {
											"x": 968,
											"y": 139,
											"width": 266,
											"height": 20
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_3_Copy-oeu2oejg.png",
											"frame": {
												"x": 968,
												"y": 139,
												"width": 266,
												"height": 20
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "62022655-4390-4FEF-9FEE-31175A013AAA",
								"kind": "group",
								"name": "Group_23",
								"originalName": "Group 2",
								"maskFrame": {
									"x": 0,
									"y": 0,
									"width": 290.9999999999982,
									"height": 45.00000000000001
								},
								"layerFrame": {
									"x": 648,
									"y": 126,
									"width": 291,
									"height": 45
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_2-njiwmji2.png",
									"frame": {
										"x": 648,
										"y": 126,
										"width": 291,
										"height": 45
									}
								},
								"children": [
									{
										"objectId": "6D792EDB-9DDD-4F1C-A62D-24E9E4E38050",
										"kind": "group",
										"name": "Group_3_Copy1",
										"originalName": "Group 3 Copy",
										"maskFrame": null,
										"layerFrame": {
											"x": 660,
											"y": 139,
											"width": 263,
											"height": 20
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_3_Copy-nkq3otjf.png",
											"frame": {
												"x": 660,
												"y": 139,
												"width": 263,
												"height": 20
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "78D0A91D-CDE3-4EEF-A994-A5C563A4A4CA",
								"kind": "group",
								"name": "Group_24",
								"originalName": "Group 2",
								"maskFrame": {
									"x": 0,
									"y": 0,
									"width": 290.9999999999977,
									"height": 45.00000000000001
								},
								"layerFrame": {
									"x": 339,
									"y": 126,
									"width": 291,
									"height": 45
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_2-nzhemee5.png",
									"frame": {
										"x": 339,
										"y": 126,
										"width": 291,
										"height": 45
									}
								},
								"children": [
									{
										"objectId": "E4596689-C62F-475E-8154-D2AF0DC1C479",
										"kind": "group",
										"name": "Group_3_Copy2",
										"originalName": "Group 3 Copy",
										"maskFrame": null,
										"layerFrame": {
											"x": 351,
											"y": 139,
											"width": 266,
											"height": 20
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_3_Copy-rtq1oty2.png",
											"frame": {
												"x": 351,
												"y": 139,
												"width": 266,
												"height": 20
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "82C4FDB4-1E36-4A0C-BF8D-6BFA95CF7E10",
								"kind": "group",
								"name": "Group_25",
								"originalName": "Group 2",
								"maskFrame": {
									"x": 0,
									"y": 0,
									"width": 290.9999999999978,
									"height": 45.00000000000001
								},
								"layerFrame": {
									"x": 31,
									"y": 126,
									"width": 291,
									"height": 45
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_2-odjdneze.png",
									"frame": {
										"x": 31,
										"y": 126,
										"width": 291,
										"height": 45
									}
								},
								"children": [
									{
										"objectId": "59799D83-B8BB-419E-A560-5798C9523F46",
										"kind": "group",
										"name": "Group_3_Copy3",
										"originalName": "Group 3 Copy",
										"maskFrame": null,
										"layerFrame": {
											"x": 43,
											"y": 139,
											"width": 268,
											"height": 20
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_3_Copy-ntk3otle.png",
											"frame": {
												"x": 43,
												"y": 139,
												"width": 268,
												"height": 20
											}
										},
										"children": []
									}
								]
							}
						]
					}
				]
			},
			{
				"objectId": "E6B4AE2D-4FAC-433A-8B5F-4BFBD161023C",
				"kind": "group",
				"name": "Group_19",
				"originalName": "Group 19",
				"maskFrame": null,
				"layerFrame": {
					"x": 1156,
					"y": 7,
					"width": 109,
					"height": 32
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-Group_19-rtzcneff.png",
					"frame": {
						"x": 1156,
						"y": 7,
						"width": 109,
						"height": 32
					}
				},
				"children": [
					{
						"objectId": "8DAEDEE9-7155-4C19-A4CB-419C85216B82",
						"kind": "group",
						"name": "global_components_buttons_default_dropdown",
						"originalName": "global-components/buttons/default/dropdown",
						"maskFrame": null,
						"layerFrame": {
							"x": 1156,
							"y": 7,
							"width": 109,
							"height": 32
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "2A8332C2-54C7-441C-A6F8-F5E46CC5AE08",
								"kind": "group",
								"name": "Group_61",
								"originalName": "Group 6",
								"maskFrame": null,
								"layerFrame": {
									"x": 1169,
									"y": 18,
									"width": 84,
									"height": 11
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_6-mke4mzmy.png",
									"frame": {
										"x": 1169,
										"y": 18,
										"width": 84,
										"height": 11
									}
								},
								"children": []
							},
							{
								"objectId": "5A58D042-1ABA-4625-8D45-6F1FF1255C46",
								"kind": "group",
								"name": "global_components_buttons_default_borders_all_rounded_corners_default",
								"originalName": "global-components/buttons/default/borders/all-rounded-corners/default",
								"maskFrame": null,
								"layerFrame": {
									"x": 1156,
									"y": 7,
									"width": 109,
									"height": 32
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-global_components_buttons_default_borders_all_rounded_corners_default-nue1oeqw.png",
									"frame": {
										"x": 1156,
										"y": 7,
										"width": 109,
										"height": 32
									}
								},
								"children": []
							}
						]
					}
				]
			},
			{
				"objectId": "7950EB43-C6C0-4367-BA96-44B983D4CF57",
				"kind": "group",
				"name": "Group1",
				"originalName": "Group",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 16,
					"width": 1281,
					"height": 32
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-Group-nzk1mevc.png",
					"frame": {
						"x": 0,
						"y": 16,
						"width": 1281,
						"height": 32
					}
				},
				"children": [
					{
						"objectId": "CA8DB731-2A27-4B9B-BF59-473754C47F41",
						"kind": "group",
						"name": "Group_12",
						"originalName": "Group 12",
						"maskFrame": null,
						"layerFrame": {
							"x": 15,
							"y": 16,
							"width": 280,
							"height": 16
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_12-q0e4rei3.png",
							"frame": {
								"x": 15,
								"y": 16,
								"width": 280,
								"height": 16
							}
						},
						"children": [
							{
								"objectId": "305AC1B2-824E-4F00-876D-0D4EA7056430",
								"kind": "group",
								"name": "project_icon1",
								"originalName": "project-icon",
								"maskFrame": null,
								"layerFrame": {
									"x": 15,
									"y": 16,
									"width": 16,
									"height": 16
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-project_icon-mza1qumx.png",
									"frame": {
										"x": 15,
										"y": 16,
										"width": 16,
										"height": 16
									}
								},
								"children": [
									{
										"objectId": "0762DCFD-DFE3-4F7F-9B36-50BAFFFBFEDA",
										"kind": "group",
										"name": "gitlab_logo1",
										"originalName": "gitlab-logo",
										"maskFrame": null,
										"layerFrame": {
											"x": 17.000000000000185,
											"y": 18.99999999999999,
											"width": 11,
											"height": 10
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-gitlab_logo-mdc2mkrd.png",
											"frame": {
												"x": 17.000000000000185,
												"y": 18.99999999999999,
												"width": 11,
												"height": 10
											}
										},
										"children": []
									}
								]
							}
						]
					}
				]
			}
		]
	},
	{
		"objectId": "454ABA93-B392-426A-9367-E89451A969AB",
		"kind": "artboard",
		"name": "view3_nav",
		"originalName": "view3_nav",
		"maskFrame": null,
		"layerFrame": {
			"x": 5670,
			"y": 0,
			"width": 1500,
			"height": 1024
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "582CAECA-0A0B-49EF-9347-FB0A2D6A78C4",
				"kind": "group",
				"name": "topbar2",
				"originalName": "topbar",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 1500,
					"height": 40
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-topbar-ntgyq0ff.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 1500,
						"height": 40
					}
				},
				"children": [
					{
						"objectId": "42EDF1F2-26FC-4808-A8F3-2F1A7C681844",
						"kind": "group",
						"name": "left_aligned2",
						"originalName": "left-aligned",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 4,
							"width": 502,
							"height": 32
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-left_aligned-ndjfreyx.png",
							"frame": {
								"x": 16,
								"y": 4,
								"width": 502,
								"height": 32
							}
						},
						"children": []
					},
					{
						"objectId": "B61A9A89-9EA3-47C6-A6CA-5120ED284FDA",
						"kind": "group",
						"name": "right_aligned2",
						"originalName": "right-aligned",
						"maskFrame": null,
						"layerFrame": {
							"x": 981,
							"y": 4,
							"width": 503,
							"height": 32
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-right_aligned-qjyxqtlb.png",
							"frame": {
								"x": 981,
								"y": 4,
								"width": 503,
								"height": 32
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "75310DAA-C737-4BD0-8014-D8A8469327E3",
				"kind": "group",
				"name": "left_sidebar2",
				"originalName": "left-sidebar",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 40,
					"width": 222,
					"height": 984
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-left_sidebar-nzuzmtbe.png",
					"frame": {
						"x": 0,
						"y": 40,
						"width": 222,
						"height": 984
					}
				},
				"children": [
					{
						"objectId": "0FB080C1-98D3-4CEB-972F-58A661FDB010",
						"kind": "group",
						"name": "top_aligned3",
						"originalName": "top-aligned",
						"maskFrame": null,
						"layerFrame": {
							"x": 0,
							"y": 52,
							"width": 220,
							"height": 409
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-top_aligned-mezcmdgw.png",
							"frame": {
								"x": 0,
								"y": 52,
								"width": 220,
								"height": 409
							}
						},
						"children": [
							{
								"objectId": "2B67D053-F6F8-42EC-BF1D-33F7AC4F141A",
								"kind": "group",
								"name": "sidebar_molecules_header1",
								"originalName": "sidebar/molecules/header",
								"maskFrame": null,
								"layerFrame": {
									"x": 8,
									"y": 52,
									"width": 118,
									"height": 40
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-sidebar_molecules_header-mki2n0qw.png",
									"frame": {
										"x": 8,
										"y": 52,
										"width": 118,
										"height": 40
									}
								},
								"children": [
									{
										"objectId": "123DA453-2A5E-4E7F-BBB0-C4FA1A542A29",
										"kind": "group",
										"name": "project_icon2",
										"originalName": "project-icon",
										"maskFrame": null,
										"layerFrame": {
											"x": 8,
											"y": 52,
											"width": 40,
											"height": 40
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-project_icon-mtizree0.png",
											"frame": {
												"x": 8,
												"y": 52,
												"width": 40,
												"height": 40
											}
										},
										"children": [
											{
												"objectId": "D3740ACC-08F6-43DA-A658-49E5264B411E",
												"kind": "group",
												"name": "gitlab_logo2",
												"originalName": "gitlab-logo",
												"maskFrame": null,
												"layerFrame": {
													"x": 15.000000000000004,
													"y": 60.00000000000001,
													"width": 26,
													"height": 24
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-gitlab_logo-rdm3ndbb.png",
													"frame": {
														"x": 15.000000000000004,
														"y": 60.00000000000001,
														"width": 26,
														"height": 24
													}
												},
												"children": []
											}
										]
									}
								]
							}
						]
					},
					{
						"objectId": "834B64F1-B66E-4F9A-A32F-78092E92D622",
						"kind": "group",
						"name": "bottom_aligned2",
						"originalName": "bottom-aligned",
						"maskFrame": null,
						"layerFrame": {
							"x": 0,
							"y": 984,
							"width": 220,
							"height": 29
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-bottom_aligned-odm0qjy0.png",
							"frame": {
								"x": 0,
								"y": 984,
								"width": 220,
								"height": 29
							}
						},
						"children": []
					}
				]
			}
		]
	},
	{
		"objectId": "44AEEDD5-2C1E-4E12-BF73-A6AF8A4D13AA",
		"kind": "artboard",
		"name": "view3_content",
		"originalName": "view3_content",
		"maskFrame": null,
		"layerFrame": {
			"x": 7270,
			"y": 0,
			"width": 1280,
			"height": 1024
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "CBFEF49D-377E-4B15-A505-C24F83A6349A",
				"kind": "group",
				"name": "Group_14",
				"originalName": "Group 14",
				"maskFrame": null,
				"layerFrame": {
					"x": -2,
					"y": 16,
					"width": 1281,
					"height": 830
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-Group_14-q0jgruy0.png",
					"frame": {
						"x": -2,
						"y": 16,
						"width": 1281,
						"height": 830
					}
				},
				"children": [
					{
						"objectId": "E4F558FB-1629-45EF-A4DC-C7585A4D9C3C",
						"kind": "group",
						"name": "chevron_down_2FAE10AF_F5C6_4550_A072_BC1EBD9DF_H1m9STUmBz",
						"originalName": "chevron-down-2FAE10AF-F5C6-4550-A072-BC1EBD9DF-H1m9STUmBz",
						"maskFrame": {
							"x": 3.923537406080868e-8,
							"y": -7.648850441910326e-10,
							"width": 7.999999991490784,
							"height": 5.000000003361741
						},
						"layerFrame": {
							"x": 30,
							"y": 810,
							"width": 6,
							"height": 9
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-chevron_down_2FAE10AF_F5C6_4550_A072_BC1EBD9DF_H1m9STUmBz-rtrgntu4.png",
							"frame": {
								"x": 30,
								"y": 810,
								"width": 6,
								"height": 9
							}
						},
						"children": []
					},
					{
						"objectId": "55E41224-9E0B-432D-91D0-636D3B9B659A",
						"kind": "group",
						"name": "chevron_down_2FAE10AF_F5C6_4550_A072_BC1EBD9DF_H1m9STUmBz1",
						"originalName": "chevron-down-2FAE10AF-F5C6-4550-A072-BC1EBD9DF-H1m9STUmBz",
						"maskFrame": {
							"x": 3.923537406080868e-8,
							"y": -7.648850441910326e-10,
							"width": 7.999999991490784,
							"height": 5.000000003361741
						},
						"layerFrame": {
							"x": 30,
							"y": 746,
							"width": 6,
							"height": 9
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-chevron_down_2FAE10AF_F5C6_4550_A072_BC1EBD9DF_H1m9STUmBz-ntvfndey.png",
							"frame": {
								"x": 30,
								"y": 746,
								"width": 6,
								"height": 9
							}
						},
						"children": []
					},
					{
						"objectId": "5DC6C8DE-4171-48A7-BF2E-866412C9FBE2",
						"kind": "group",
						"name": "chevron_down_2FAE10AF_F5C6_4550_A072_BC1EBD9DF_H1m9STUmBz2",
						"originalName": "chevron-down-2FAE10AF-F5C6-4550-A072-BC1EBD9DF-H1m9STUmBz",
						"maskFrame": {
							"x": 3.923560143448412e-8,
							"y": -7.646576705155894e-10,
							"width": 7.999999991490784,
							"height": 5.000000003361741
						},
						"layerFrame": {
							"x": 248,
							"y": 565,
							"width": 8,
							"height": 5
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-chevron_down_2FAE10AF_F5C6_4550_A072_BC1EBD9DF_H1m9STUmBz-nurdnkm4.png",
							"frame": {
								"x": 248,
								"y": 565,
								"width": 8,
								"height": 5
							}
						},
						"children": []
					},
					{
						"objectId": "4128A62C-6A22-4062-B176-8091BF7FFDC1",
						"kind": "group",
						"name": "chevron_down_2FAE10AF_F5C6_4550_A072_BC1EBD9DF_H1m9STUmBz3",
						"originalName": "chevron-down-2FAE10AF-F5C6-4550-A072-BC1EBD9DF-H1m9STUmBz",
						"maskFrame": {
							"x": 3.923560143448412e-8,
							"y": -7.646576705155894e-10,
							"width": 7.999999991490784,
							"height": 5.000000003361741
						},
						"layerFrame": {
							"x": 29,
							"y": 619,
							"width": 8,
							"height": 5
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-chevron_down_2FAE10AF_F5C6_4550_A072_BC1EBD9DF_H1m9STUmBz-ndeyoee2.png",
							"frame": {
								"x": 29,
								"y": 619,
								"width": 8,
								"height": 5
							}
						},
						"children": []
					},
					{
						"objectId": "421DDF64-2FF4-48FF-84E3-7AD60DEFBE51",
						"kind": "group",
						"name": "arrow_right_CBB2F8E5_6E4B_4A1F_A4A6_1824B02C54_rJV1q41kSf",
						"originalName": "arrow-right-CBB2F8E5-6E4B-4A1F-A4A6-1824B02C54-rJV1q41kSf",
						"maskFrame": {
							"x": 0,
							"y": 0,
							"width": 13.98621226001154,
							"height": 9.872942560011545
						},
						"layerFrame": {
							"x": 125,
							"y": 189,
							"width": 10,
							"height": 14
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-arrow_right_CBB2F8E5_6E4B_4A1F_A4A6_1824B02C54_rJV1q41kSf-ndixrerg.png",
							"frame": {
								"x": 125,
								"y": 189,
								"width": 10,
								"height": 14
							}
						},
						"children": []
					},
					{
						"objectId": "87CA1D31-656E-4057-93B5-59B5E2172B26",
						"kind": "group",
						"name": "Area_chart",
						"originalName": "Area chart",
						"maskFrame": null,
						"layerFrame": {
							"x": 78,
							"y": 290,
							"width": 1184,
							"height": 131
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Area_chart-odddqtfe.png",
							"frame": {
								"x": 78,
								"y": 290,
								"width": 1184,
								"height": 131
							}
						},
						"children": []
					},
					{
						"objectId": "FD4403C7-3C60-4558-BF7B-516548C99F06",
						"kind": "group",
						"name": "Area_chart1",
						"originalName": "Area chart",
						"maskFrame": null,
						"layerFrame": {
							"x": 78,
							"y": 290,
							"width": 1184,
							"height": 130
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Area_chart-rkq0ndaz.png",
							"frame": {
								"x": 78,
								"y": 290,
								"width": 1184,
								"height": 130
							}
						},
						"children": []
					},
					{
						"objectId": "6BDA5B19-388A-4D26-9349-C5363699D4FB",
						"kind": "group",
						"name": "Area_chart2",
						"originalName": "Area chart",
						"maskFrame": null,
						"layerFrame": {
							"x": 78,
							"y": 289,
							"width": 1184,
							"height": 131
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Area_chart-nkjeqtvc.png",
							"frame": {
								"x": 78,
								"y": 289,
								"width": 1184,
								"height": 131
							}
						},
						"children": []
					},
					{
						"objectId": "79118E63-FFE0-47CB-AFA4-6EB7E0547137",
						"kind": "group",
						"name": "Group_152",
						"originalName": "Group 15",
						"maskFrame": null,
						"layerFrame": {
							"x": 452,
							"y": 82,
							"width": 12,
							"height": 12
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_15-nzkxmthf.png",
							"frame": {
								"x": 452,
								"y": 82,
								"width": 12,
								"height": 12
							}
						},
						"children": []
					},
					{
						"objectId": "C590162E-FB17-4F3F-B9B3-0D306724DD62",
						"kind": "group",
						"name": "Group_111",
						"originalName": "Group 11",
						"maskFrame": null,
						"layerFrame": {
							"x": -2,
							"y": 16,
							"width": 1281,
							"height": 830
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_11-qzu5mde2.png",
							"frame": {
								"x": -2,
								"y": 16,
								"width": 1281,
								"height": 830
							}
						},
						"children": [
							{
								"objectId": "3676F6D3-43BA-4078-9621-68B2ED087680",
								"kind": "group",
								"name": "Group_62",
								"originalName": "Group 6",
								"maskFrame": null,
								"layerFrame": {
									"x": 13,
									"y": 256,
									"width": 1249,
									"height": 206
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_6-mzy3nky2.png",
									"frame": {
										"x": 13,
										"y": 256,
										"width": 1249,
										"height": 206
									}
								},
								"children": [
									{
										"objectId": "2DAB0C4C-06D3-4D5E-94CD-A23E9F762ED5",
										"kind": "group",
										"name": "Stacked_area_chart",
										"originalName": "Stacked area chart",
										"maskFrame": null,
										"layerFrame": {
											"x": 78,
											"y": 335,
											"width": 1183,
											"height": 86
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Stacked_area_chart-mkrbqjbd.png",
											"frame": {
												"x": 78,
												"y": 335,
												"width": 1183,
												"height": 86
											}
										},
										"children": []
									},
									{
										"objectId": "40F3288B-DCE2-4394-AA4F-BF1B83A1C26B",
										"kind": "group",
										"name": "x_axis2",
										"originalName": "x-axis",
										"maskFrame": null,
										"layerFrame": {
											"x": 77,
											"y": 420,
											"width": 1185,
											"height": 42
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-x_axis-ndbgmzi4.png",
											"frame": {
												"x": 77,
												"y": 420,
												"width": 1185,
												"height": 42
											}
										},
										"children": []
									},
									{
										"objectId": "C995BE05-52E9-4632-9A48-3B6DA48D8748",
										"kind": "group",
										"name": "y_axis2",
										"originalName": "y-axis",
										"maskFrame": null,
										"layerFrame": {
											"x": 13,
											"y": 256,
											"width": 98,
											"height": 171
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-y_axis-qzk5nujf.png",
											"frame": {
												"x": 13,
												"y": 256,
												"width": 98,
												"height": 171
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "98C7A8B0-166F-4057-AE81-AE85C6754FDB",
								"kind": "group",
								"name": "Group_63",
								"originalName": "Group 6",
								"maskFrame": null,
								"layerFrame": {
									"x": 13,
									"y": 256,
									"width": 1249,
									"height": 309
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_6-othdn0e4.png",
									"frame": {
										"x": 13,
										"y": 256,
										"width": 1249,
										"height": 309
									}
								},
								"children": [
									{
										"objectId": "A58503E5-F321-42A0-85B6-CC2DBF2478B1",
										"kind": "group",
										"name": "Stacked_area_chart1",
										"originalName": "Stacked area chart",
										"maskFrame": null,
										"layerFrame": {
											"x": 78,
											"y": 300,
											"width": 1183,
											"height": 223
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Stacked_area_chart-qtu4ntaz.png",
											"frame": {
												"x": 78,
												"y": 300,
												"width": 1183,
												"height": 223
											}
										},
										"children": []
									},
									{
										"objectId": "F794E7BC-DEBB-4BF3-88CE-756505A49EC1",
										"kind": "group",
										"name": "x_axis3",
										"originalName": "x-axis",
										"maskFrame": null,
										"layerFrame": {
											"x": 74,
											"y": 522,
											"width": 1188,
											"height": 42
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-x_axis-rjc5neu3.png",
											"frame": {
												"x": 74,
												"y": 522,
												"width": 1188,
												"height": 42
											}
										},
										"children": []
									},
									{
										"objectId": "57CF394A-B783-453F-8D80-3362EBF91F21",
										"kind": "group",
										"name": "y_axis3",
										"originalName": "y-axis",
										"maskFrame": null,
										"layerFrame": {
											"x": 13,
											"y": 259,
											"width": 98,
											"height": 269
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-y_axis-ntddrjm5.png",
											"frame": {
												"x": 13,
												"y": 259,
												"width": 98,
												"height": 269
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "7D4020B9-BDC3-4C29-8317-DE9358CC44ED",
								"kind": "group",
								"name": "Env_Stage_Copy2",
								"originalName": "Env_Stage Copy",
								"maskFrame": null,
								"layerFrame": {
									"x": 13,
									"y": 495,
									"width": 1248,
									"height": 351
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Env_Stage_Copy-n0q0mdiw.png",
									"frame": {
										"x": 13,
										"y": 495,
										"width": 1248,
										"height": 351
									}
								},
								"children": [
									{
										"objectId": "33A7F566-2023-45AB-9D65-EDBB133D1444",
										"kind": "group",
										"name": "global_components_tables_headers",
										"originalName": "global-components/tables/headers",
										"maskFrame": null,
										"layerFrame": {
											"x": 14,
											"y": 560,
											"width": 1246,
											"height": 29
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-global_components_tables_headers-mznbn0y1.png",
											"frame": {
												"x": 14,
												"y": 560,
												"width": 1246,
												"height": 29
											}
										},
										"children": [
											{
												"objectId": "0E9AFECF-A2DE-4DAA-875C-7CE1162C6421",
												"kind": "group",
												"name": "column",
												"originalName": "column",
												"maskFrame": null,
												"layerFrame": {
													"x": 179,
													"y": 560,
													"width": 899,
													"height": 14
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-column-meu5quzf.png",
													"frame": {
														"x": 179,
														"y": 560,
														"width": 899,
														"height": 14
													}
												},
												"children": []
											},
											{
												"objectId": "0897A4C3-5471-4EB1-B722-25AE102DD234",
												"kind": "group",
												"name": "column1",
												"originalName": "column",
												"maskFrame": null,
												"layerFrame": {
													"x": 341,
													"y": 541,
													"width": 256,
													"height": 48
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-column-mdg5n0e0.png",
													"frame": {
														"x": 341,
														"y": 541,
														"width": 256,
														"height": 48
													}
												},
												"children": []
											},
											{
												"objectId": "C2E0C5BC-355A-4F78-8406-0444460525E1",
												"kind": "group",
												"name": "column2",
												"originalName": "column",
												"maskFrame": null,
												"layerFrame": {
													"x": 153,
													"y": 541,
													"width": 172,
													"height": 48
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-column-qzjfmem1.png",
													"frame": {
														"x": 153,
														"y": 541,
														"width": 172,
														"height": 48
													}
												},
												"children": []
											},
											{
												"objectId": "D2775CDF-9E81-4E5E-8543-5AC70FF18BAE",
												"kind": "group",
												"name": "column3",
												"originalName": "column",
												"maskFrame": null,
												"layerFrame": {
													"x": 30,
													"y": 561,
													"width": 36,
													"height": 11
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-column-rdi3nzvd.png",
													"frame": {
														"x": 30,
														"y": 561,
														"width": 36,
														"height": 11
													}
												},
												"children": []
											},
											{
												"objectId": "25C048F8-C449-4AD5-AE8E-C26507A48972",
												"kind": "group",
												"name": "global_components_backgrounds_transparent_border_bottom_dark",
												"originalName": "global-components/backgrounds/transparent/border-bottom--dark",
												"maskFrame": null,
												"layerFrame": {
													"x": 14,
													"y": 588,
													"width": 1246,
													"height": 1
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-global_components_backgrounds_transparent_border_bottom_dark-mjvdmdq4.png",
													"frame": {
														"x": 14,
														"y": 588,
														"width": 1246,
														"height": 1
													}
												},
												"children": []
											}
										]
									},
									{
										"objectId": "22D5E562-482C-433D-909D-187B613DD84A",
										"kind": "group",
										"name": "icn_external_link",
										"originalName": "icn/external-link",
										"maskFrame": {
											"x": 0,
											"y": 0,
											"width": 13.88378589717223,
											"height": 13.88382999346642
										},
										"layerFrame": {
											"x": 837,
											"y": 614,
											"width": 14,
											"height": 14
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-icn_external_link-mjjenuu1.png",
											"frame": {
												"x": 837,
												"y": 614,
												"width": 14,
												"height": 14
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "AC12A94B-D4C5-4E0C-B60B-6D5DED580C3B",
								"kind": "group",
								"name": "box",
								"originalName": "box",
								"maskFrame": null,
								"layerFrame": {
									"x": 1067,
									"y": 64,
									"width": 195,
									"height": 160
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-box-qumxmke5.png",
									"frame": {
										"x": 1067,
										"y": 64,
										"width": 195,
										"height": 160
									}
								},
								"children": []
							},
							{
								"objectId": "F7AB94F2-C3B8-4BE8-8BBA-87E3BA960DDC",
								"kind": "group",
								"name": "box1",
								"originalName": "box",
								"maskFrame": null,
								"layerFrame": {
									"x": 856,
									"y": 64,
									"width": 195,
									"height": 160
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-box-rjdbqjk0.png",
									"frame": {
										"x": 856,
										"y": 64,
										"width": 195,
										"height": 160
									}
								},
								"children": []
							},
							{
								"objectId": "198477E5-3859-4F29-8F16-008653008844",
								"kind": "group",
								"name": "box2",
								"originalName": "box",
								"maskFrame": null,
								"layerFrame": {
									"x": 645,
									"y": 64,
									"width": 195,
									"height": 160
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-box-mtk4ndc3.png",
									"frame": {
										"x": 645,
										"y": 64,
										"width": 195,
										"height": 160
									}
								},
								"children": []
							},
							{
								"objectId": "5A798DEE-A051-45E2-B366-2640E66BF433",
								"kind": "group",
								"name": "box3",
								"originalName": "box",
								"maskFrame": null,
								"layerFrame": {
									"x": 435,
									"y": 64,
									"width": 195,
									"height": 160
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-box-nue3othe.png",
									"frame": {
										"x": 435,
										"y": 64,
										"width": 195,
										"height": 160
									}
								},
								"children": []
							},
							{
								"objectId": "43BF3C1E-8FC5-4958-87AF-3C6B0F373B5A",
								"kind": "group",
								"name": "box4",
								"originalName": "box",
								"maskFrame": null,
								"layerFrame": {
									"x": 13,
									"y": 64,
									"width": 195,
									"height": 160
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-box-ndncrjnd.png",
									"frame": {
										"x": 13,
										"y": 64,
										"width": 195,
										"height": 160
									}
								},
								"children": []
							},
							{
								"objectId": "80DA8EC8-7B44-4C65-9F44-EB4443BF051E",
								"kind": "group",
								"name": "box5",
								"originalName": "box",
								"maskFrame": null,
								"layerFrame": {
									"x": 224,
									"y": 64,
									"width": 195,
									"height": 160
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-box-odbeqthf.png",
									"frame": {
										"x": 224,
										"y": 64,
										"width": 195,
										"height": 160
									}
								},
								"children": []
							},
							{
								"objectId": "F1C795AF-E216-4203-9E8A-7BF244CA9216",
								"kind": "group",
								"name": "Group_132",
								"originalName": "Group 13",
								"maskFrame": null,
								"layerFrame": {
									"x": 13,
									"y": 127,
									"width": 1248,
									"height": 124
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "70460BB1-D8C3-4814-B57B-4CBDBB6B8658",
										"kind": "group",
										"name": "Env_Stage_Copy4",
										"originalName": "Env_Stage Copy",
										"maskFrame": null,
										"layerFrame": {
											"x": 13,
											"y": 127,
											"width": 1248,
											"height": 124
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Env_Stage_Copy-nza0njbc.png",
											"frame": {
												"x": 13,
												"y": 127,
												"width": 1248,
												"height": 124
											}
										},
										"children": [
											{
												"objectId": "B05998C9-B6D6-4C22-8718-DDAD317F223A",
												"kind": "group",
												"name": "Group_26",
												"originalName": "Group 2",
												"maskFrame": {
													"x": 0,
													"y": 0,
													"width": 290.9999999999978,
													"height": 45.00000000000001
												},
												"layerFrame": {
													"x": 954,
													"y": 189,
													"width": 291,
													"height": 45
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_2-qja1otk4.png",
													"frame": {
														"x": 954,
														"y": 189,
														"width": 291,
														"height": 45
													}
												},
												"children": [
													{
														"objectId": "69961D7A-AF9D-488A-BF4E-209D613AB859",
														"kind": "group",
														"name": "Group_3_Copy4",
														"originalName": "Group 3 Copy",
														"maskFrame": null,
														"layerFrame": {
															"x": 966,
															"y": 202,
															"width": 266,
															"height": 20
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_3_Copy-njk5njfe.png",
															"frame": {
																"x": 966,
																"y": 202,
																"width": 266,
																"height": 20
															}
														},
														"children": []
													}
												]
											},
											{
												"objectId": "7E5B2B24-5471-442F-A7ED-4A2B53835719",
												"kind": "group",
												"name": "Group_27",
												"originalName": "Group 2",
												"maskFrame": {
													"x": 0,
													"y": 0,
													"width": 290.9999999999982,
													"height": 45.00000000000001
												},
												"layerFrame": {
													"x": 646,
													"y": 189,
													"width": 291,
													"height": 45
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_2-n0u1qjjc.png",
													"frame": {
														"x": 646,
														"y": 189,
														"width": 291,
														"height": 45
													}
												},
												"children": [
													{
														"objectId": "10E181B6-5CA1-44B1-8CE1-EB3DA0A857E4",
														"kind": "group",
														"name": "Group_3_Copy5",
														"originalName": "Group 3 Copy",
														"maskFrame": null,
														"layerFrame": {
															"x": 658,
															"y": 202,
															"width": 263,
															"height": 20
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_3_Copy-mtbfmtgx.png",
															"frame": {
																"x": 658,
																"y": 202,
																"width": 263,
																"height": 20
															}
														},
														"children": []
													}
												]
											},
											{
												"objectId": "40EA271D-6FCE-4596-9816-4CB91ED1F6DD",
												"kind": "group",
												"name": "Group_28",
												"originalName": "Group 2",
												"maskFrame": {
													"x": 0,
													"y": 0,
													"width": 290.9999999999977,
													"height": 45.00000000000001
												},
												"layerFrame": {
													"x": 337,
													"y": 189,
													"width": 291,
													"height": 45
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_2-ndbfqti3.png",
													"frame": {
														"x": 337,
														"y": 189,
														"width": 291,
														"height": 45
													}
												},
												"children": [
													{
														"objectId": "1EEFB981-7943-403B-AD80-482280962EA7",
														"kind": "group",
														"name": "Group_3_Copy6",
														"originalName": "Group 3 Copy",
														"maskFrame": null,
														"layerFrame": {
															"x": 349,
															"y": 202,
															"width": 266,
															"height": 20
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_3_Copy-muvfrki5.png",
															"frame": {
																"x": 349,
																"y": 202,
																"width": 266,
																"height": 20
															}
														},
														"children": []
													}
												]
											},
											{
												"objectId": "68AEB7F1-9EE2-4B63-869B-91085B19F67A",
												"kind": "group",
												"name": "Group_29",
												"originalName": "Group 2",
												"maskFrame": {
													"x": 0,
													"y": 0,
													"width": 290.9999999999978,
													"height": 45.00000000000001
												},
												"layerFrame": {
													"x": 29,
													"y": 189,
													"width": 291,
													"height": 45
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_2-njhbrui3.png",
													"frame": {
														"x": 29,
														"y": 189,
														"width": 291,
														"height": 45
													}
												},
												"children": [
													{
														"objectId": "534E7629-2BCA-42B9-B296-81209A881322",
														"kind": "group",
														"name": "Group_3_Copy7",
														"originalName": "Group 3 Copy",
														"maskFrame": null,
														"layerFrame": {
															"x": 41,
															"y": 202,
															"width": 268,
															"height": 20
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_3_Copy-ntm0rtc2.png",
															"frame": {
																"x": 41,
																"y": 202,
																"width": 268,
																"height": 20
															}
														},
														"children": []
													}
												]
											}
										]
									}
								]
							},
							{
								"objectId": "9369CB70-5162-4596-BA5A-A88C34466D1C",
								"kind": "group",
								"name": "Env_Stage_Copy3",
								"originalName": "Env_Stage Copy",
								"maskFrame": null,
								"layerFrame": {
									"x": 434,
									"y": 397,
									"width": 405,
									"height": 300
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Env_Stage_Copy-otm2ounc.png",
									"frame": {
										"x": 434,
										"y": 397,
										"width": 405,
										"height": 300
									}
								},
								"children": [
									{
										"objectId": "71E61CCF-6881-44A6-9265-559ECA767319",
										"kind": "group",
										"name": "Env_1",
										"originalName": "Env_1",
										"maskFrame": null,
										"layerFrame": {
											"x": 441,
											"y": 453,
											"width": 393,
											"height": 176
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Env_1-nzffnjfd.png",
											"frame": {
												"x": 441,
												"y": 453,
												"width": 393,
												"height": 176
											}
										},
										"children": [
											{
												"objectId": "5B3024B3-6EDA-4FB3-9A0A-EE9A836D13D0",
												"kind": "group",
												"name": "Group_101",
												"originalName": "Group 10",
												"maskFrame": null,
												"layerFrame": {
													"x": 450,
													"y": 470,
													"width": 292,
													"height": 153
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_10-nuizmdi0.png",
													"frame": {
														"x": 450,
														"y": 470,
														"width": 292,
														"height": 153
													}
												},
												"children": [
													{
														"objectId": "87D9DF3A-0AE6-4352-BCBE-31295CEB6A18",
														"kind": "group",
														"name": "Group_4_Copy_3",
														"originalName": "Group 4 Copy 3",
														"maskFrame": null,
														"layerFrame": {
															"x": 458,
															"y": 577,
															"width": 139,
															"height": 19
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_4_Copy_3-oddeourg.png",
															"frame": {
																"x": 458,
																"y": 577,
																"width": 139,
																"height": 19
															}
														},
														"children": []
													},
													{
														"objectId": "BED092AA-A828-4513-AF66-E1D14E3E8D33",
														"kind": "text",
														"name": "Group_4_Copy_2",
														"originalName": "Group 4 Copy 2",
														"maskFrame": null,
														"layerFrame": {
															"x": 474,
															"y": 555,
															"width": 145,
															"height": 11
														},
														"visible": true,
														"metadata": {
															"opacity": 1,
															"string": "Error rate exceeded 0.1%",
															"css": [
																"/* Error rate exceeded: */",
																"font-family: SourceSansPro-Regular;",
																"font-size: 14px;",
																"color: #5C5C5C;",
																"letter-spacing: 0;"
															]
														},
														"image": {
															"path": "images/Layer-Group_4_Copy_2-qkvemdky.png",
															"frame": {
																"x": 474,
																"y": 555,
																"width": 145,
																"height": 11
															}
														},
														"children": []
													},
													{
														"objectId": "04765CBC-1FE8-4169-B71B-9E2D00276AA3",
														"kind": "group",
														"name": "Group_42",
														"originalName": "Group 4",
														"maskFrame": null,
														"layerFrame": {
															"x": 450,
															"y": 551,
															"width": 18,
															"height": 18
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_4-mdq3njvd.png",
															"frame": {
																"x": 450,
																"y": 551,
																"width": 18,
																"height": 18
															}
														},
														"children": []
													},
													{
														"objectId": "B6CFC055-64CA-4FF1-AB7A-571F8FA34378",
														"kind": "group",
														"name": "Group_4_Copy",
														"originalName": "Group 4 Copy",
														"maskFrame": null,
														"layerFrame": {
															"x": 473,
															"y": 527,
															"width": 239,
															"height": 18
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_4_Copy-qjzdrkmw.png",
															"frame": {
																"x": 473,
																"y": 527,
																"width": 239,
																"height": 18
															}
														},
														"children": [
															{
																"objectId": "21FC3146-886B-45B1-BDE1-0CB97E994AAB",
																"kind": "group",
																"name": "Group_55",
																"originalName": "Group 5",
																"maskFrame": {
																	"x": 0,
																	"y": 0,
																	"width": 16,
																	"height": 16
																},
																"layerFrame": {
																	"x": 527,
																	"y": 527,
																	"width": 18,
																	"height": 18
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_5-mjfgqzmx.png",
																	"frame": {
																		"x": 527,
																		"y": 527,
																		"width": 18,
																		"height": 18
																	}
																},
																"children": []
															}
														]
													},
													{
														"objectId": "8AE143E6-A3C4-4845-86A3-10F1BFC01C15",
														"kind": "group",
														"name": "Group_9_Copy",
														"originalName": "Group 9 Copy",
														"maskFrame": null,
														"layerFrame": {
															"x": 450,
															"y": 526,
															"width": 18,
															"height": 18
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_9_Copy-oeffmtqz.png",
															"frame": {
																"x": 450,
																"y": 526,
																"width": 18,
																"height": 18
															}
														},
														"children": []
													},
													{
														"objectId": "F2134635-C8BF-4F15-AA97-9F335550BF2A",
														"kind": "group",
														"name": "Group_71",
														"originalName": "Group 7",
														"maskFrame": null,
														"layerFrame": {
															"x": 491,
															"y": 614,
															"width": 251,
															"height": 5
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"children": [
															{
																"objectId": "C81E5420-8E75-4B41-8013-A4B084DCBAFF",
																"kind": "group",
																"name": "Group_91",
																"originalName": "Group 9",
																"maskFrame": null,
																"layerFrame": {
																	"x": 491,
																	"y": 614,
																	"width": 251,
																	"height": 5
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "09A71CE6-59EC-476A-B9C7-2D0FE8ED61E0",
																		"kind": "group",
																		"name": "Group_81",
																		"originalName": "Group 8",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 491,
																			"y": 614,
																			"width": 251,
																			"height": 5
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-Group_8-mdlbnzfd.png",
																			"frame": {
																				"x": 491,
																				"y": 614,
																				"width": 251,
																				"height": 5
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													}
												]
											},
											{
												"objectId": "AC09157B-B056-48EA-99C4-3EBF4A18DF4A",
												"kind": "group",
												"name": "Group_6_Copy",
												"originalName": "Group 6 Copy",
												"maskFrame": null,
												"layerFrame": {
													"x": 690,
													"y": 463,
													"width": 133,
													"height": 25
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"children": [
													{
														"objectId": "7E5C5F79-C162-4141-B8F2-BDACDF714FC8",
														"kind": "group",
														"name": "Group_5_Copy_2",
														"originalName": "Group 5 Copy 2",
														"maskFrame": null,
														"layerFrame": {
															"x": 772,
															"y": 463,
															"width": 51,
															"height": 25
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_5_Copy_2-n0u1qzvg.png",
															"frame": {
																"x": 772,
																"y": 463,
																"width": 51,
																"height": 25
															}
														},
														"children": []
													},
													{
														"objectId": "D904315F-0B27-49C5-BA3C-65EA15E8CE6B",
														"kind": "group",
														"name": "Group_5_Copy_4",
														"originalName": "Group 5 Copy 4",
														"maskFrame": null,
														"layerFrame": {
															"x": 751,
															"y": 463,
															"width": 72,
															"height": 25
														},
														"visible": false,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_5_Copy_4-rdkwndmx.png",
															"frame": {
																"x": 751,
																"y": 463,
																"width": 72,
																"height": 25
															}
														},
														"children": []
													},
													{
														"objectId": "BBFADAF1-C320-4B4C-BF22-0818511F6583",
														"kind": "group",
														"name": "Group_5_Copy_3",
														"originalName": "Group 5 Copy 3",
														"maskFrame": null,
														"layerFrame": {
															"x": 690,
															"y": 463,
															"width": 72,
															"height": 25
														},
														"visible": true,
														"metadata": {
															"opacity": 0.5
														},
														"image": {
															"path": "images/Layer-Group_5_Copy_3-qkjgqurb.png",
															"frame": {
																"x": 690,
																"y": 463,
																"width": 72,
																"height": 25
															}
														},
														"children": []
													}
												]
											}
										]
									}
								]
							},
							{
								"objectId": "5E5B89DA-38CF-4658-B1DA-7335024A3649",
								"kind": "group",
								"name": "Env_Stage",
								"originalName": "Env_Stage",
								"maskFrame": {
									"x": 0,
									"y": 0,
									"width": 405,
									"height": 300
								},
								"layerFrame": {
									"x": 13,
									"y": 397,
									"width": 405,
									"height": 300
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Env_Stage-nuu1qjg5.png",
									"frame": {
										"x": 13,
										"y": 397,
										"width": 405,
										"height": 300
									}
								},
								"children": [
									{
										"objectId": "D00E7642-D207-4D70-88BF-E67FEE3A1F04",
										"kind": "group",
										"name": "Env_2",
										"originalName": "Env_2",
										"maskFrame": null,
										"layerFrame": {
											"x": 21,
											"y": 453,
											"width": 388,
											"height": 105
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Env_2-rdawrtc2.png",
											"frame": {
												"x": 21,
												"y": 453,
												"width": 388,
												"height": 105
											}
										},
										"children": [
											{
												"objectId": "C2466072-3167-468A-9F65-0AE0D22D663C",
												"kind": "group",
												"name": "Group_18",
												"originalName": "Group 18",
												"maskFrame": null,
												"layerFrame": {
													"x": 30,
													"y": 470,
													"width": 121,
													"height": 75
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_18-qzi0njyw.png",
													"frame": {
														"x": 30,
														"y": 470,
														"width": 121,
														"height": 75
													}
												},
												"children": [
													{
														"objectId": "92561B4D-90E8-45AC-BEB5-F611ED12EDD1",
														"kind": "group",
														"name": "Group_4_Copy1",
														"originalName": "Group 4 Copy",
														"maskFrame": null,
														"layerFrame": {
															"x": 53,
															"y": 527,
															"width": 72,
															"height": 18
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_4_Copy-oti1njfc.png",
															"frame": {
																"x": 53,
																"y": 527,
																"width": 72,
																"height": 18
															}
														},
														"children": [
															{
																"objectId": "E4F505D8-0DEF-41EB-A7CC-D4A70CF1046C",
																"kind": "group",
																"name": "Group_56",
																"originalName": "Group 5",
																"maskFrame": {
																	"x": 0,
																	"y": 0,
																	"width": 16,
																	"height": 16
																},
																"layerFrame": {
																	"x": 107,
																	"y": 527,
																	"width": 18,
																	"height": 18
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_5-rtrgnta1.png",
																	"frame": {
																		"x": 107,
																		"y": 527,
																		"width": 18,
																		"height": 18
																	}
																},
																"children": []
															}
														]
													},
													{
														"objectId": "D7134B89-6DDB-4F80-BAD9-707FB4C8225B",
														"kind": "group",
														"name": "Group_9_Copy1",
														"originalName": "Group 9 Copy",
														"maskFrame": null,
														"layerFrame": {
															"x": 30,
															"y": 526,
															"width": 18,
															"height": 18
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_9_Copy-rdcxmzrc.png",
															"frame": {
																"x": 30,
																"y": 526,
																"width": 18,
																"height": 18
															}
														},
														"children": []
													}
												]
											},
											{
												"objectId": "5DC8C928-CD39-4184-8C58-01FE4E76B215",
												"kind": "group",
												"name": "Group_6_Copy1",
												"originalName": "Group 6 Copy",
												"maskFrame": null,
												"layerFrame": {
													"x": 266,
													"y": 463,
													"width": 51,
													"height": 25
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"children": [
													{
														"objectId": "E569D9CF-8E52-4C47-9B72-514814488D1F",
														"kind": "group",
														"name": "Group_5_Copy_21",
														"originalName": "Group 5 Copy 2",
														"maskFrame": null,
														"layerFrame": {
															"x": 266,
															"y": 463,
															"width": 51,
															"height": 25
														},
														"visible": false,
														"metadata": {
															"opacity": 0.5
														},
														"image": {
															"path": "images/Layer-Group_5_Copy_2-rtu2ouq5.png",
															"frame": {
																"x": 266,
																"y": 463,
																"width": 51,
																"height": 25
															}
														},
														"children": []
													}
												]
											}
										]
									},
									{
										"objectId": "0279951B-D591-4988-B81C-3F52B19581A6",
										"kind": "group",
										"name": "Env_21",
										"originalName": "Env_2",
										"maskFrame": null,
										"layerFrame": {
											"x": 21,
											"y": 568,
											"width": 388,
											"height": 103
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Env_2-mdi3otk1.png",
											"frame": {
												"x": 21,
												"y": 568,
												"width": 388,
												"height": 103
											}
										},
										"children": [
											{
												"objectId": "89618C68-4B1A-4FEF-BE34-B44F6751568E",
												"kind": "group",
												"name": "Group_17",
												"originalName": "Group 17",
												"maskFrame": null,
												"layerFrame": {
													"x": 30,
													"y": 585,
													"width": 111,
													"height": 75
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_17-odk2mthd.png",
													"frame": {
														"x": 30,
														"y": 585,
														"width": 111,
														"height": 75
													}
												},
												"children": [
													{
														"objectId": "14C4146D-E70A-4FC9-AA46-CC3C41962D4F",
														"kind": "group",
														"name": "Group_4_Copy2",
														"originalName": "Group 4 Copy",
														"maskFrame": null,
														"layerFrame": {
															"x": 53,
															"y": 642,
															"width": 72,
															"height": 18
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_4_Copy-mtrdnde0.png",
															"frame": {
																"x": 53,
																"y": 642,
																"width": 72,
																"height": 18
															}
														},
														"children": [
															{
																"objectId": "8CF4A8EE-B8E8-40AC-BADC-00B669CE07D0",
																"kind": "group",
																"name": "Group_57",
																"originalName": "Group 5",
																"maskFrame": {
																	"x": 0,
																	"y": 0,
																	"width": 16,
																	"height": 16
																},
																"layerFrame": {
																	"x": 107,
																	"y": 642,
																	"width": 18,
																	"height": 18
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_5-oengnee4.png",
																	"frame": {
																		"x": 107,
																		"y": 642,
																		"width": 18,
																		"height": 18
																	}
																},
																"children": []
															}
														]
													},
													{
														"objectId": "F0A13748-8EBA-4986-9235-49B8D2F40079",
														"kind": "group",
														"name": "Group_9_Copy2",
														"originalName": "Group 9 Copy",
														"maskFrame": null,
														"layerFrame": {
															"x": 30,
															"y": 641,
															"width": 18,
															"height": 18
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_9_Copy-rjbbmtm3.png",
															"frame": {
																"x": 30,
																"y": 641,
																"width": 18,
																"height": 18
															}
														},
														"children": []
													}
												]
											}
										]
									}
								]
							},
							{
								"objectId": "6B56C118-3EA6-481D-96A9-514EB73F544D",
								"kind": "group",
								"name": "Env_Stage1",
								"originalName": "Env_Stage",
								"maskFrame": {
									"x": 0,
									"y": 0,
									"width": 406,
									"height": 300
								},
								"layerFrame": {
									"x": 855,
									"y": 397,
									"width": 406,
									"height": 300
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Env_Stage-nki1nkmx.png",
									"frame": {
										"x": 855,
										"y": 397,
										"width": 406,
										"height": 300
									}
								},
								"children": [
									{
										"objectId": "D768ADCC-2334-4F68-80F8-D8AAC2EDBA23",
										"kind": "group",
										"name": "Env_22",
										"originalName": "Env_2",
										"maskFrame": null,
										"layerFrame": {
											"x": 863,
											"y": 453,
											"width": 388,
											"height": 128
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Env_2-rdc2oefe.png",
											"frame": {
												"x": 863,
												"y": 453,
												"width": 388,
												"height": 128
											}
										},
										"children": [
											{
												"objectId": "3C770554-CED4-4505-9478-AF5F5777FB25",
												"kind": "group",
												"name": "Group_181",
												"originalName": "Group 18",
												"maskFrame": null,
												"layerFrame": {
													"x": 872,
													"y": 470,
													"width": 304,
													"height": 105
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_18-m0m3nza1.png",
													"frame": {
														"x": 872,
														"y": 470,
														"width": 304,
														"height": 105
													}
												},
												"children": [
													{
														"objectId": "13446834-3270-4DF7-8873-02E281E33DC2",
														"kind": "group",
														"name": "Group_4_Copy3",
														"originalName": "Group 4 Copy",
														"maskFrame": null,
														"layerFrame": {
															"x": 895,
															"y": 527,
															"width": 281,
															"height": 18
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_4_Copy-mtm0ndy4.png",
															"frame": {
																"x": 895,
																"y": 527,
																"width": 281,
																"height": 18
															}
														},
														"children": [
															{
																"objectId": "D1752D76-1082-4252-8128-E1B02E98C4B0",
																"kind": "group",
																"name": "Group_58",
																"originalName": "Group 5",
																"maskFrame": {
																	"x": 0,
																	"y": 0,
																	"width": 16,
																	"height": 16
																},
																"layerFrame": {
																	"x": 949,
																	"y": 527,
																	"width": 18,
																	"height": 18
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_5-rde3ntje.png",
																	"frame": {
																		"x": 949,
																		"y": 527,
																		"width": 18,
																		"height": 18
																	}
																},
																"children": []
															}
														]
													},
													{
														"objectId": "F8D26916-80CB-4F65-BF36-CBAF8E38934F",
														"kind": "group",
														"name": "Group_9_Copy3",
														"originalName": "Group 9 Copy",
														"maskFrame": null,
														"layerFrame": {
															"x": 872,
															"y": 526,
															"width": 18,
															"height": 18
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_9_Copy-rjhemjy5.png",
															"frame": {
																"x": 872,
																"y": 526,
																"width": 18,
																"height": 18
															}
														},
														"children": []
													},
													{
														"objectId": "28D88DC0-6791-4CE5-9177-648D06522CAA",
														"kind": "group",
														"name": "Group_72",
														"originalName": "Group 7",
														"maskFrame": null,
														"layerFrame": {
															"x": 918,
															"y": 566,
															"width": 233,
															"height": 5
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"children": [
															{
																"objectId": "E7A2C876-2403-4154-B736-867545E53588",
																"kind": "group",
																"name": "Group_92",
																"originalName": "Group 9",
																"maskFrame": null,
																"layerFrame": {
																	"x": 918,
																	"y": 566,
																	"width": 233,
																	"height": 5
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "98E6DCB6-7D95-410A-BE2F-D55996772732",
																		"kind": "group",
																		"name": "Group_82",
																		"originalName": "Group 8",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 918,
																			"y": 566,
																			"width": 233,
																			"height": 5
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-Group_8-othfnkrd.png",
																			"frame": {
																				"x": 918,
																				"y": 566,
																				"width": 233,
																				"height": 5
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													}
												]
											},
											{
												"objectId": "775EE6AE-3A33-448D-B9D2-A38333ACEEBC",
												"kind": "group",
												"name": "Group_6_Copy2",
												"originalName": "Group 6 Copy",
												"maskFrame": null,
												"layerFrame": {
													"x": 1086,
													"y": 463,
													"width": 154,
													"height": 25
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"children": [
													{
														"objectId": "6585B4D9-4E25-428E-A6A6-93EE7FCFA4FA",
														"kind": "group",
														"name": "Group_5_Copy_22",
														"originalName": "Group 5 Copy 2",
														"maskFrame": null,
														"layerFrame": {
															"x": 1107,
															"y": 463,
															"width": 51,
															"height": 25
														},
														"visible": false,
														"metadata": {
															"opacity": 0.5
														},
														"image": {
															"path": "images/Layer-Group_5_Copy_2-nju4nui0.png",
															"frame": {
																"x": 1107,
																"y": 463,
																"width": 51,
																"height": 25
															}
														},
														"children": []
													},
													{
														"objectId": "C5851D0D-5928-41EB-8364-F589F890ABDF",
														"kind": "group",
														"name": "Group_5_Copy_41",
														"originalName": "Group 5 Copy 4",
														"maskFrame": null,
														"layerFrame": {
															"x": 1168,
															"y": 463,
															"width": 72,
															"height": 25
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_5_Copy_4-qzu4ntfe.png",
															"frame": {
																"x": 1168,
																"y": 463,
																"width": 72,
																"height": 25
															}
														},
														"children": []
													},
													{
														"objectId": "5859C77F-4EAD-4CB0-B062-3A9C5707A8D9",
														"kind": "group",
														"name": "Group_5_Copy_31",
														"originalName": "Group 5 Copy 3",
														"maskFrame": null,
														"layerFrame": {
															"x": 1086,
															"y": 463,
															"width": 72,
															"height": 25
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_5_Copy_3-ntg1oum3.png",
															"frame": {
																"x": 1086,
																"y": 463,
																"width": 72,
																"height": 25
															}
														},
														"children": []
													}
												]
											}
										]
									},
									{
										"objectId": "18245EA1-7BFF-40D6-AD75-F4D5787516CD",
										"kind": "group",
										"name": "Env_23",
										"originalName": "Env_2",
										"maskFrame": null,
										"layerFrame": {
											"x": 863,
											"y": 591,
											"width": 388,
											"height": 128
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Env_2-mtgyndvf.png",
											"frame": {
												"x": 863,
												"y": 591,
												"width": 388,
												"height": 128
											}
										},
										"children": [
											{
												"objectId": "D16A4EF5-C882-4B5F-B50A-0F2CBF77589F",
												"kind": "group",
												"name": "Group_73",
												"originalName": "Group 7",
												"maskFrame": null,
												"layerFrame": {
													"x": 917,
													"y": 704,
													"width": 227,
													"height": 5
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"children": [
													{
														"objectId": "C02E3982-4A70-47DD-BFCC-DC670C4C59F0",
														"kind": "group",
														"name": "Group_93",
														"originalName": "Group 9",
														"maskFrame": null,
														"layerFrame": {
															"x": 917,
															"y": 704,
															"width": 227,
															"height": 5
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"children": [
															{
																"objectId": "CAF752BA-1B83-4842-85BD-EA866A0AA667",
																"kind": "group",
																"name": "Group_83",
																"originalName": "Group 8",
																"maskFrame": null,
																"layerFrame": {
																	"x": 917,
																	"y": 704,
																	"width": 227,
																	"height": 5
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_8-q0fgnzuy.png",
																	"frame": {
																		"x": 917,
																		"y": 704,
																		"width": 227,
																		"height": 5
																	}
																},
																"children": []
															}
														]
													}
												]
											},
											{
												"objectId": "7C452D7A-D930-4622-AE89-25578D250639",
												"kind": "group",
												"name": "Group_6_Copy3",
												"originalName": "Group 6 Copy",
												"maskFrame": null,
												"layerFrame": {
													"x": 1169,
													"y": 601,
													"width": 72,
													"height": 25
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"children": [
													{
														"objectId": "9F5D1C6F-049F-4D3C-99AA-782A5D1AB414",
														"kind": "group",
														"name": "Group_5_Copy_32",
														"originalName": "Group 5 Copy 3",
														"maskFrame": null,
														"layerFrame": {
															"x": 1169,
															"y": 601,
															"width": 72,
															"height": 25
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_5_Copy_3-ouy1rdfd.png",
															"frame": {
																"x": 1169,
																"y": 601,
																"width": 72,
																"height": 25
															}
														},
														"children": []
													}
												]
											},
											{
												"objectId": "26DB12AA-0F22-4C1F-A9C0-65C603F90FAF",
												"kind": "group",
												"name": "Group_171",
												"originalName": "Group 17",
												"maskFrame": null,
												"layerFrame": {
													"x": 872,
													"y": 608,
													"width": 303,
													"height": 75
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_17-mjzeqjey.png",
													"frame": {
														"x": 872,
														"y": 608,
														"width": 303,
														"height": 75
													}
												},
												"children": [
													{
														"objectId": "DE496FAE-574E-426A-806C-2D9953A23AE5",
														"kind": "group",
														"name": "Group_4_Copy4",
														"originalName": "Group 4 Copy",
														"maskFrame": null,
														"layerFrame": {
															"x": 895,
															"y": 665,
															"width": 280,
															"height": 18
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_4_Copy-reu0otzg.png",
															"frame": {
																"x": 895,
																"y": 665,
																"width": 280,
																"height": 18
															}
														},
														"children": [
															{
																"objectId": "857DBC8E-2808-4A1A-A172-3C1C98C2EB98",
																"kind": "group",
																"name": "Group_59",
																"originalName": "Group 5",
																"maskFrame": {
																	"x": 0,
																	"y": 0,
																	"width": 16,
																	"height": 16
																},
																"layerFrame": {
																	"x": 949,
																	"y": 665,
																	"width": 18,
																	"height": 18
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_5-odu3rejd.png",
																	"frame": {
																		"x": 949,
																		"y": 665,
																		"width": 18,
																		"height": 18
																	}
																},
																"children": []
															}
														]
													},
													{
														"objectId": "7B712E00-2076-4BFA-B58C-3254D59F920E",
														"kind": "group",
														"name": "Group_9_Copy4",
														"originalName": "Group 9 Copy",
														"maskFrame": null,
														"layerFrame": {
															"x": 872,
															"y": 664,
															"width": 18,
															"height": 18
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_9_Copy-n0i3mtjf.png",
															"frame": {
																"x": 872,
																"y": 664,
																"width": 18,
																"height": 18
															}
														},
														"children": []
													}
												]
											}
										]
									}
								]
							},
							{
								"objectId": "BC8DBF2F-6012-4F35-8044-927E40E827B0",
								"kind": "group",
								"name": "Group_33",
								"originalName": "Group 3",
								"maskFrame": null,
								"layerFrame": {
									"x": 15,
									"y": 64,
									"width": 1249,
									"height": 45
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": []
							},
							{
								"objectId": "F47BF1F5-AE6A-4B29-9B54-25EB1964EDAB",
								"kind": "group",
								"name": "Group_191",
								"originalName": "Group 19",
								"maskFrame": null,
								"layerFrame": {
									"x": -1,
									"y": 46,
									"width": 1281,
									"height": 66
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_19-rjq3qkyx.png",
									"frame": {
										"x": -1,
										"y": 46,
										"width": 1281,
										"height": 66
									}
								},
								"children": [
									{
										"objectId": "099B670E-84DF-4A0C-A6A0-0953E046589B",
										"kind": "group",
										"name": "global_components_buttons_default_dropdown1",
										"originalName": "global-components/buttons/default/dropdown",
										"maskFrame": null,
										"layerFrame": {
											"x": 15,
											"y": 63,
											"width": 109,
											"height": 32
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-global_components_buttons_default_dropdown-mdk5qjy3.png",
											"frame": {
												"x": 15,
												"y": 63,
												"width": 109,
												"height": 32
											}
										},
										"children": [
											{
												"objectId": "AEF9B56A-DBC9-4C11-8A48-E95E07C4DB65",
												"kind": "group",
												"name": "global_components_buttons_default_borders_all_rounded_corners_default1",
												"originalName": "global-components/buttons/default/borders/all-rounded-corners/default",
												"maskFrame": null,
												"layerFrame": {
													"x": 15,
													"y": 63,
													"width": 109,
													"height": 32
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-global_components_buttons_default_borders_all_rounded_corners_default-quvgoui1.png",
													"frame": {
														"x": 15,
														"y": 63,
														"width": 109,
														"height": 32
													}
												},
												"children": []
											}
										]
									}
								]
							},
							{
								"objectId": "B8C3D8B8-B493-4359-882B-4C7304C997D5",
								"kind": "group",
								"name": "Group2",
								"originalName": "Group",
								"maskFrame": null,
								"layerFrame": {
									"x": -2,
									"y": 16,
									"width": 1281,
									"height": 32
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group-qjhdm0q4.png",
									"frame": {
										"x": -2,
										"y": 16,
										"width": 1281,
										"height": 32
									}
								},
								"children": [
									{
										"objectId": "6B4C0A18-7D58-4146-B99D-A76C7C869EF1",
										"kind": "group",
										"name": "Group_121",
										"originalName": "Group 12",
										"maskFrame": null,
										"layerFrame": {
											"x": 13,
											"y": 16,
											"width": 257,
											"height": 16
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_12-nki0qzbb.png",
											"frame": {
												"x": 13,
												"y": 16,
												"width": 257,
												"height": 16
											}
										},
										"children": [
											{
												"objectId": "5E6B35C0-52D0-4170-8F6B-F44E80A58FF5",
												"kind": "group",
												"name": "project_icon3",
												"originalName": "project-icon",
												"maskFrame": null,
												"layerFrame": {
													"x": 13,
													"y": 16,
													"width": 16,
													"height": 16
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-project_icon-nuu2qjm1.png",
													"frame": {
														"x": 13,
														"y": 16,
														"width": 16,
														"height": 16
													}
												},
												"children": [
													{
														"objectId": "E9C3490C-D67C-4CE9-8A70-F3988437C865",
														"kind": "group",
														"name": "gitlab_logo3",
														"originalName": "gitlab-logo",
														"maskFrame": null,
														"layerFrame": {
															"x": 15.000000000000185,
															"y": 18.99999999999999,
															"width": 11,
															"height": 10
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-gitlab_logo-rtldmzq5.png",
															"frame": {
																"x": 15.000000000000185,
																"y": 18.99999999999999,
																"width": 11,
																"height": 10
															}
														},
														"children": []
													}
												]
											}
										]
									}
								]
							}
						]
					}
				]
			}
		]
	},
	{
		"objectId": "5F144F31-2C0B-4E1B-A323-0F92A0FAB2EF",
		"kind": "artboard",
		"name": "view4_nav",
		"originalName": "view4_nav",
		"maskFrame": null,
		"layerFrame": {
			"x": 8650,
			"y": 0,
			"width": 1500,
			"height": 1024
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "7A74CF29-ADF6-4DC6-83E2-27CCE3CAB02F",
				"kind": "group",
				"name": "topbar3",
				"originalName": "topbar",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 1500,
					"height": 40
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-topbar-n0e3neng.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 1500,
						"height": 40
					}
				},
				"children": [
					{
						"objectId": "6580E3ED-6622-4352-92EF-4035A744D168",
						"kind": "group",
						"name": "left_aligned3",
						"originalName": "left-aligned",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 4,
							"width": 502,
							"height": 32
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-left_aligned-nju4meuz.png",
							"frame": {
								"x": 16,
								"y": 4,
								"width": 502,
								"height": 32
							}
						},
						"children": []
					},
					{
						"objectId": "E7A4E5A9-63E6-4106-86D6-5AF5855DBE19",
						"kind": "group",
						"name": "right_aligned3",
						"originalName": "right-aligned",
						"maskFrame": null,
						"layerFrame": {
							"x": 1010,
							"y": 4,
							"width": 474,
							"height": 32
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-right_aligned-rtdbneu1.png",
							"frame": {
								"x": 1010,
								"y": 4,
								"width": 474,
								"height": 32
							}
						},
						"children": [
							{
								"objectId": "970EC54D-8485-465D-829D-9A4015356D0D",
								"kind": "group",
								"name": "user_items1",
								"originalName": "user-items",
								"maskFrame": null,
								"layerFrame": {
									"x": 1298,
									"y": 8,
									"width": 186,
									"height": 24
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-user_items-otcwrum1.png",
									"frame": {
										"x": 1298,
										"y": 8,
										"width": 186,
										"height": 24
									}
								},
								"children": []
							}
						]
					}
				]
			},
			{
				"objectId": "41A0F958-62FE-4538-8D62-A2B23A9428AC",
				"kind": "group",
				"name": "right_sidebar1",
				"originalName": "right-sidebar",
				"maskFrame": null,
				"layerFrame": {
					"x": 1210,
					"y": 40,
					"width": 290,
					"height": 984
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-right_sidebar-ndfbmey5.png",
					"frame": {
						"x": 1210,
						"y": 40,
						"width": 290,
						"height": 984
					}
				},
				"children": [
					{
						"objectId": "F00BB0CC-AFF1-4491-9752-BBB228DE1306",
						"kind": "group",
						"name": "top_aligned4",
						"originalName": "top-aligned",
						"maskFrame": null,
						"layerFrame": {
							"x": 1227,
							"y": 56,
							"width": 257,
							"height": 648
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-top_aligned-rjawqkiw.png",
							"frame": {
								"x": 1227,
								"y": 56,
								"width": 257,
								"height": 648
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "994E5431-61BC-459F-9F17-9D239CCF7610",
				"kind": "group",
				"name": "left_sidebar3",
				"originalName": "left-sidebar",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 40,
					"width": 220,
					"height": 984
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-left_sidebar-otk0rtu0.png",
					"frame": {
						"x": 0,
						"y": 40,
						"width": 220,
						"height": 984
					}
				},
				"children": [
					{
						"objectId": "49B738DE-4E98-42A8-8A84-16B6E829A88B",
						"kind": "group",
						"name": "top_aligned5",
						"originalName": "top-aligned",
						"maskFrame": null,
						"layerFrame": {
							"x": 0,
							"y": 48,
							"width": 220,
							"height": 417
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-top_aligned-ndlcnzm4.png",
							"frame": {
								"x": 0,
								"y": 48,
								"width": 220,
								"height": 417
							}
						},
						"children": []
					},
					{
						"objectId": "27A658AA-EC36-4FCD-A4B1-2FAE1B728A56",
						"kind": "group",
						"name": "bottom_aligned3",
						"originalName": "bottom-aligned",
						"maskFrame": null,
						"layerFrame": {
							"x": 0,
							"y": 976,
							"width": 220,
							"height": 33
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-bottom_aligned-mjdbnju4.png",
							"frame": {
								"x": 0,
								"y": 976,
								"width": 220,
								"height": 33
							}
						},
						"children": []
					}
				]
			}
		]
	},
	{
		"objectId": "3CF98754-AF2D-43ED-80A0-752342D5A6FF",
		"kind": "artboard",
		"name": "view4_content",
		"originalName": "view4_content",
		"maskFrame": null,
		"layerFrame": {
			"x": 10250,
			"y": 0,
			"width": 990,
			"height": 1674
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "5D0712B7-9E3D-4FA9-B9A7-D3FC0E149466",
				"kind": "group",
				"name": "below1",
				"originalName": "below",
				"maskFrame": null,
				"layerFrame": {
					"x": 13,
					"y": 1238,
					"width": 965,
					"height": 438
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "D0440A3B-566D-4F2F-8733-A00D4FB1F36F",
						"kind": "group",
						"name": "Group_112",
						"originalName": "Group 11",
						"maskFrame": null,
						"layerFrame": {
							"x": 13,
							"y": 1238,
							"width": 965,
							"height": 438
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_11-rda0ndbb.png",
							"frame": {
								"x": 13,
								"y": 1238,
								"width": 965,
								"height": 438
							}
						},
						"children": [
							{
								"objectId": "63834B6D-86FB-4DB9-B531-0425B3F24C23",
								"kind": "group",
								"name": "issuable_row_emoji1",
								"originalName": "issuable__row--emoji",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 1238,
									"width": 958,
									"height": 64
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-issuable_row_emoji-njm4mzrc.png",
									"frame": {
										"x": 16,
										"y": 1238,
										"width": 958,
										"height": 64
									}
								},
								"children": [
									{
										"objectId": "14D71D36-8411-49DC-ACF7-0990CCD736E3",
										"kind": "group",
										"name": "issue_actions1",
										"originalName": "issue-actions",
										"maskFrame": null,
										"layerFrame": {
											"x": 782,
											"y": 1254,
											"width": 192,
											"height": 32
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-issue_actions-mtrenzfe.png",
											"frame": {
												"x": 782,
												"y": 1254,
												"width": 192,
												"height": 32
											}
										},
										"children": []
									},
									{
										"objectId": "B3DB5E48-17B5-4819-A399-BD1D7FE5E01D",
										"kind": "group",
										"name": "emoji_actions1",
										"originalName": "emoji-actions",
										"maskFrame": null,
										"layerFrame": {
											"x": 16,
											"y": 1254,
											"width": 174,
											"height": 32
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-emoji_actions-qjneqjvf.png",
											"frame": {
												"x": 16,
												"y": 1254,
												"width": 174,
												"height": 32
											}
										},
										"children": []
									}
								]
							}
						]
					}
				]
			},
			{
				"objectId": "06E3DFD4-E083-40F1-986A-8E63A682230B",
				"kind": "group",
				"name": "row_revert",
				"originalName": "row_revert",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 1138,
					"width": 958,
					"height": 84
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-row_revert-mdzfm0rg.png",
					"frame": {
						"x": 16,
						"y": 1138,
						"width": 958,
						"height": 84
					}
				},
				"children": [
					{
						"objectId": "AB54EF11-D381-4D97-9897-BB866F392DE4",
						"kind": "group",
						"name": "secondary_row1",
						"originalName": "secondary-row",
						"maskFrame": null,
						"layerFrame": {
							"x": 17,
							"y": 1194,
							"width": 956,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-secondary_row-qui1nevg.png",
							"frame": {
								"x": 17,
								"y": 1194,
								"width": 956,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "01C68465-FD35-4DD4-B962-7441ED11DC20",
						"kind": "group",
						"name": "Column31",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 363,
							"y": 1138,
							"width": 610,
							"height": 56
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-mdfdnjg0.png",
							"frame": {
								"x": 363,
								"y": 1138,
								"width": 610,
								"height": 56
							}
						},
						"children": []
					},
					{
						"objectId": "C93B1033-562F-4DDA-993C-8A22E439F76C",
						"kind": "group",
						"name": "Column32",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 33,
							"y": 1154,
							"width": 455,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-qzkzqjew.png",
							"frame": {
								"x": 33,
								"y": 1154,
								"width": 455,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "A20475D7-2FCC-4654-AF00-7D13530E3320",
						"kind": "group",
						"name": "Column33",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 32,
							"y": 1138,
							"width": 121,
							"height": 56
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-qtiwndc1.png",
							"frame": {
								"x": 32,
								"y": 1138,
								"width": 121,
								"height": 56
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "C8809A77-8CD4-4697-B0E1-CCB6F9576FB7",
				"kind": "group",
				"name": "row_deploy1",
				"originalName": "row_deploy",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 1055,
					"width": 958,
					"height": 84
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-row_deploy-qzg4mdlb.png",
					"frame": {
						"x": 16,
						"y": 1055,
						"width": 958,
						"height": 84
					}
				},
				"children": [
					{
						"objectId": "CB77DECC-2A47-4C25-B375-60AF3B216E55",
						"kind": "group",
						"name": "Column34",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 890,
							"y": 1071,
							"width": 67,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-q0i3n0rf.png",
							"frame": {
								"x": 890,
								"y": 1071,
								"width": 67,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "BB12B9E7-0C96-4846-B6ED-6CCB52ACD9B4",
						"kind": "group",
						"name": "Column35",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 365,
							"y": 1055,
							"width": 272,
							"height": 56
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-qkixmki5.png",
							"frame": {
								"x": 365,
								"y": 1055,
								"width": 272,
								"height": 56
							}
						},
						"children": []
					},
					{
						"objectId": "F5E7462F-CDE7-4D61-9A47-09DB7EEAACEF",
						"kind": "group",
						"name": "Column36",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 33,
							"y": 1071,
							"width": 613,
							"height": 58
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-rjvfnzq2.png",
							"frame": {
								"x": 33,
								"y": 1071,
								"width": 613,
								"height": 58
							}
						},
						"children": []
					},
					{
						"objectId": "06802065-BC9F-470C-93CA-D726DD77273B",
						"kind": "group",
						"name": "Column37",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 33,
							"y": 1071,
							"width": 24,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-mdy4mdiw.png",
							"frame": {
								"x": 33,
								"y": 1071,
								"width": 24,
								"height": 24
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "7DE3CA20-DA6C-4ED6-83D8-25EDD81825E7",
				"kind": "group",
				"name": "row_merge1",
				"originalName": "row_merge",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 900,
					"width": 958,
					"height": 156
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-row_merge-n0rfm0nb.png",
					"frame": {
						"x": 16,
						"y": 900,
						"width": 958,
						"height": 156
					}
				},
				"children": [
					{
						"objectId": "D360D3D5-5646-4CF8-93D4-8EEC21AFB73D",
						"kind": "group",
						"name": "secondary_row2",
						"originalName": "secondary-row",
						"maskFrame": null,
						"layerFrame": {
							"x": 17,
							"y": 956,
							"width": 956,
							"height": 35
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-secondary_row-rdm2meqz.png",
							"frame": {
								"x": 17,
								"y": 956,
								"width": 956,
								"height": 35
							}
						},
						"children": []
					},
					{
						"objectId": "F9F6CAD2-7095-4035-BF1A-3396F854D59C",
						"kind": "group",
						"name": "Column38",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 363,
							"y": 900,
							"width": 610,
							"height": 56
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-rjlgnknb.png",
							"frame": {
								"x": 363,
								"y": 900,
								"width": 610,
								"height": 56
							}
						},
						"children": []
					},
					{
						"objectId": "BF0E3356-5451-4E7F-B9C6-76E17E6A2A20",
						"kind": "group",
						"name": "Column39",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 32,
							"y": 918,
							"width": 378,
							"height": 20
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-qkywrtmz.png",
							"frame": {
								"x": 32,
								"y": 918,
								"width": 378,
								"height": 20
							}
						},
						"children": []
					},
					{
						"objectId": "63F45092-2271-40BE-9E04-4AF0FEED394D",
						"kind": "group",
						"name": "Column40",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 32,
							"y": 900,
							"width": 121,
							"height": 56
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-njngnduw.png",
							"frame": {
								"x": 32,
								"y": 900,
								"width": 121,
								"height": 56
							}
						},
						"children": []
					},
					{
						"objectId": "0ADC8B89-9CAD-4FD0-9D5C-8ED2DFC8AB28",
						"kind": "group",
						"name": "Column41",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 17,
							"y": 900,
							"width": 56,
							"height": 56
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-mefeqzhc.png",
							"frame": {
								"x": 17,
								"y": 900,
								"width": 56,
								"height": 56
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "93D3550D-45A3-4F06-BE41-00717D5A368C",
				"kind": "group",
				"name": "row_licensefinder1",
				"originalName": "row_licensefinder",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 845,
					"width": 958,
					"height": 56
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-row_licensefinder-otnemzu1.png",
					"frame": {
						"x": 16,
						"y": 845,
						"width": 958,
						"height": 56
					}
				},
				"children": [
					{
						"objectId": "8FBF9C9C-DFBB-4D2D-85B8-A17B6EE824F7",
						"kind": "group",
						"name": "Column42",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 637,
							"y": 845,
							"width": 336,
							"height": 56
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-oezcrjld.png",
							"frame": {
								"x": 637,
								"y": 845,
								"width": 336,
								"height": 56
							}
						},
						"children": []
					},
					{
						"objectId": "EC49A8FF-6058-4587-8C34-183382E52C43",
						"kind": "group",
						"name": "Column43",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 368,
							"y": 845,
							"width": 269,
							"height": 56
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-rum0oue4.png",
							"frame": {
								"x": 368,
								"y": 845,
								"width": 269,
								"height": 56
							}
						},
						"children": []
					},
					{
						"objectId": "D027829B-9EA6-417A-A4D0-35804637DAC9",
						"kind": "group",
						"name": "Column44",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 39,
							"y": 867,
							"width": 454,
							"height": 15
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-rdaynzgy.png",
							"frame": {
								"x": 39,
								"y": 867,
								"width": 454,
								"height": 15
							}
						},
						"children": [
							{
								"objectId": "B32A2516-0D2E-4C47-9C52-1C3D50235003",
								"kind": "group",
								"name": "Group_34",
								"originalName": "Group 3",
								"maskFrame": null,
								"layerFrame": {
									"x": 39,
									"y": 867,
									"width": 13,
									"height": 13
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_3-qjmyqti1.png",
									"frame": {
										"x": 39,
										"y": 867,
										"width": 13,
										"height": 13
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "4862E164-0F9D-45BE-A208-12820FFD7F4D",
						"kind": "group",
						"name": "Column45",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 33,
							"y": 861,
							"width": 24,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-ndg2mkux.png",
							"frame": {
								"x": 33,
								"y": 861,
								"width": 24,
								"height": 24
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "F969D810-57ED-4DE8-95B4-8391FA55A408",
				"kind": "group",
				"name": "row_performancemetrics1",
				"originalName": "row_performancemetrics",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 789,
					"width": 958,
					"height": 57
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-row_performancemetrics-rjk2ouq4.png",
					"frame": {
						"x": 16,
						"y": 789,
						"width": 958,
						"height": 57
					}
				},
				"children": [
					{
						"objectId": "D873577C-8B0D-49A5-B700-D6AECA387261",
						"kind": "group",
						"name": "row_performancemetrics_button_expand1",
						"originalName": "row_performancemetrics_button_expand",
						"maskFrame": null,
						"layerFrame": {
							"x": 893,
							"y": 805,
							"width": 64,
							"height": 24
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-row_performancemetrics_button_expand-rdg3mzu3.png",
							"frame": {
								"x": 893,
								"y": 805,
								"width": 64,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "5B55ED94-5C5B-454A-B6E4-A1C10BC3EDBF",
						"kind": "group",
						"name": "row_performancemetrics_button_collapse1",
						"originalName": "row_performancemetrics_button_collapse",
						"maskFrame": null,
						"layerFrame": {
							"x": 886,
							"y": 805,
							"width": 71,
							"height": 24
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-row_performancemetrics_button_collapse-nui1nuve.png",
							"frame": {
								"x": 886,
								"y": 805,
								"width": 71,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "31C887C1-A876-4651-9182-718E4FBECAD5",
						"kind": "group",
						"name": "Column46",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 368,
							"y": 790,
							"width": 269,
							"height": 56
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-mzfdodg3.png",
							"frame": {
								"x": 368,
								"y": 790,
								"width": 269,
								"height": 56
							}
						},
						"children": []
					},
					{
						"objectId": "C6EDF2D0-34E6-49D5-8BAA-CA7FF670A7BC",
						"kind": "group",
						"name": "Column47",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 74,
							"y": 813,
							"width": 265,
							"height": 14
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-qzzfreyy.png",
							"frame": {
								"x": 74,
								"y": 813,
								"width": 265,
								"height": 14
							}
						},
						"children": []
					},
					{
						"objectId": "0F52FE66-CB8D-4951-B1E2-B838DB1AE80D",
						"kind": "group",
						"name": "Column48",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 33,
							"y": 806,
							"width": 24,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-mey1mkzf.png",
							"frame": {
								"x": 33,
								"y": 806,
								"width": 24,
								"height": 24
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "F58C7CC8-9DC7-4469-A9D0-EF54BF715526",
				"kind": "group",
				"name": "row_securitytests_expanded1",
				"originalName": "row_securitytests_expanded",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 523,
					"width": 958,
					"height": 267
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "3F1C251D-3EA9-4DEC-855B-45294DEB297F",
						"kind": "group",
						"name": "Group_74",
						"originalName": "Group 7",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 523,
							"width": 958,
							"height": 267
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_7-m0yxqzi1.png",
							"frame": {
								"x": 16,
								"y": 523,
								"width": 958,
								"height": 267
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "46705B7F-54E0-49D2-8615-DA6CA09CB518",
				"kind": "group",
				"name": "row_securitytests1",
				"originalName": "row_securitytests",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 468,
					"width": 958,
					"height": 56
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-row_securitytests-ndy3mdvc.png",
					"frame": {
						"x": 16,
						"y": 468,
						"width": 958,
						"height": 56
					}
				},
				"children": [
					{
						"objectId": "A22B1C72-4E0A-42E1-8C1C-3C966E8098EA",
						"kind": "group",
						"name": "row_securitytests_button_expand1",
						"originalName": "row_securitytests_button_expand",
						"maskFrame": null,
						"layerFrame": {
							"x": 893,
							"y": 483,
							"width": 64,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-row_securitytests_button_expand-qtiyqjfd.png",
							"frame": {
								"x": 893,
								"y": 483,
								"width": 64,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "5B6AD048-F7F2-4A7D-818D-90EF97092994",
						"kind": "group",
						"name": "row_securitytests_button_collapse1",
						"originalName": "row_securitytests_button_collapse",
						"maskFrame": null,
						"layerFrame": {
							"x": 886,
							"y": 483,
							"width": 71,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-row_securitytests_button_collapse-nui2quqw.png",
							"frame": {
								"x": 886,
								"y": 483,
								"width": 71,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "412D0D0E-A355-401A-BA09-7E798766F9F6",
						"kind": "group",
						"name": "Column49",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 368,
							"y": 468,
							"width": 269,
							"height": 56
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-ndeyrdbe.png",
							"frame": {
								"x": 368,
								"y": 468,
								"width": 269,
								"height": 56
							}
						},
						"children": []
					},
					{
						"objectId": "43E26658-F34E-4963-A56B-C273E17D4E2E",
						"kind": "group",
						"name": "Column50",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 72,
							"y": 468,
							"width": 296,
							"height": 56
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-ndnfmjy2.png",
							"frame": {
								"x": 72,
								"y": 468,
								"width": 296,
								"height": 56
							}
						},
						"children": []
					},
					{
						"objectId": "19E44721-3CEB-413F-AAFD-46AE8CDE96F1",
						"kind": "text",
						"name": "Column51",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 33,
							"y": 484,
							"width": 24,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1,
							"string": "",
							"css": [
								"/* : */",
								"font-family: FontAwesome;",
								"font-size: 24px;",
								"color: #2E2E2E;",
								"line-height: 16px;"
							]
						},
						"image": {
							"path": "images/Layer-Column-mtlfndq3.png",
							"frame": {
								"x": 33,
								"y": 484,
								"width": 24,
								"height": 24
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "A5F14107-6E0B-4971-A5B2-92519FD1176E",
				"kind": "group",
				"name": "row_codequality1",
				"originalName": "row_codequality",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 412,
					"width": 958,
					"height": 57
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-row_codequality-qtvgmtqx.png",
					"frame": {
						"x": 16,
						"y": 412,
						"width": 958,
						"height": 57
					}
				},
				"children": [
					{
						"objectId": "457A6E0A-8C8D-4F2E-A6DC-6C9340C92C16",
						"kind": "group",
						"name": "row_codequality_button_expand1",
						"originalName": "row_codequality_button_expand",
						"maskFrame": null,
						"layerFrame": {
							"x": 893,
							"y": 428,
							"width": 64,
							"height": 24
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-row_codequality_button_expand-ndu3qtzf.png",
							"frame": {
								"x": 893,
								"y": 428,
								"width": 64,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "1CBC251E-BF00-47BD-B66E-6BAD7CD09F2C",
						"kind": "group",
						"name": "row_codequality_button_collapse1",
						"originalName": "row_codequality_button_collapse",
						"maskFrame": null,
						"layerFrame": {
							"x": 886,
							"y": 428,
							"width": 71,
							"height": 24
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-row_codequality_button_collapse-muncqzi1.png",
							"frame": {
								"x": 886,
								"y": 428,
								"width": 71,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "A4C87C87-0520-4DA6-897A-22174BF41284",
						"kind": "group",
						"name": "Column52",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 369,
							"y": 413,
							"width": 269,
							"height": 56
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-qtrdoddd.png",
							"frame": {
								"x": 369,
								"y": 413,
								"width": 269,
								"height": 56
							}
						},
						"children": []
					},
					{
						"objectId": "A185D35D-DE8B-47D8-85AE-D0727660A637",
						"kind": "group",
						"name": "Column53",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 74,
							"y": 436,
							"width": 215,
							"height": 14
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-qte4nuqz.png",
							"frame": {
								"x": 74,
								"y": 436,
								"width": 215,
								"height": 14
							}
						},
						"children": []
					},
					{
						"objectId": "0475162D-1F7F-4434-A6A9-5B5BAFF39F37",
						"kind": "group",
						"name": "Column54",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 34,
							"y": 429,
							"width": 24,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-mdq3nte2.png",
							"frame": {
								"x": 34,
								"y": 429,
								"width": 24,
								"height": 24
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "80D03ACE-43EE-4E05-A605-67C81995DBC2",
				"kind": "group",
				"name": "row_testsummary1",
				"originalName": "row_testsummary",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 357,
					"width": 958,
					"height": 56
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-row_testsummary-odbemdnb.png",
					"frame": {
						"x": 16,
						"y": 357,
						"width": 958,
						"height": 56
					}
				},
				"children": [
					{
						"objectId": "652AC24F-0359-45C9-9207-E785B265323A",
						"kind": "group",
						"name": "Column55",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 270,
							"y": 357,
							"width": 272,
							"height": 56
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-njuyqumy.png",
							"frame": {
								"x": 270,
								"y": 357,
								"width": 272,
								"height": 56
							}
						},
						"children": []
					},
					{
						"objectId": "A1A498A5-4422-41B3-8D47-AD1F1D550C3A",
						"kind": "group",
						"name": "Column56",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 72,
							"y": 381,
							"width": 192,
							"height": 13
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-qtfbndk4.png",
							"frame": {
								"x": 72,
								"y": 381,
								"width": 192,
								"height": 13
							}
						},
						"children": []
					},
					{
						"objectId": "46453392-D3A2-43B2-BFC8-AB0BFA5E62D2",
						"kind": "group",
						"name": "Column57",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 33,
							"y": 373,
							"width": 24,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-ndy0ntmz.png",
							"frame": {
								"x": 33,
								"y": 373,
								"width": 24,
								"height": 24
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "49CA257A-AECB-4358-A41F-233C3391E476",
				"kind": "group",
				"name": "row_pipeline1",
				"originalName": "row_pipeline",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 302,
					"width": 958,
					"height": 56
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-row_pipeline-ndldqti1.png",
					"frame": {
						"x": 16,
						"y": 302,
						"width": 958,
						"height": 56
					}
				},
				"children": [
					{
						"objectId": "1E796060-B0D4-42EB-8594-1E1975B12914",
						"kind": "group",
						"name": "row_pipeline_button_expand1",
						"originalName": "row_pipeline_button_expand",
						"maskFrame": null,
						"layerFrame": {
							"x": 893,
							"y": 318,
							"width": 64,
							"height": 24
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-row_pipeline_button_expand-muu3otyw.png",
							"frame": {
								"x": 893,
								"y": 318,
								"width": 64,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "FBB57C5E-1BD2-4619-8BA8-F38A9FA69619",
						"kind": "group",
						"name": "row_pipeline_button_artifacts2",
						"originalName": "row_pipeline_button_artifacts",
						"maskFrame": null,
						"layerFrame": {
							"x": 816,
							"y": 318,
							"width": 141,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-row_pipeline_button_artifacts-rkjcntdd.png",
							"frame": {
								"x": 816,
								"y": 318,
								"width": 141,
								"height": 24
							}
						},
						"children": [
							{
								"objectId": "5037174D-FBB8-4927-A2ED-6EF4B802A068",
								"kind": "group",
								"name": "caret2",
								"originalName": "caret",
								"maskFrame": {
									"x": 0.2410714285714284,
									"y": 0,
									"width": 3.374999999999997,
									"height": 8
								},
								"layerFrame": {
									"x": 939.9999999999992,
									"y": 329,
									"width": 9,
									"height": 4
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-caret-ntaznze3.png",
									"frame": {
										"x": 939.9999999999992,
										"y": 329,
										"width": 9,
										"height": 4
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "3DF25142-B56A-4076-B9BC-F58C8CCD492E",
						"kind": "group",
						"name": "row_pipeline_button_artifacts3",
						"originalName": "row_pipeline_button_artifacts",
						"maskFrame": null,
						"layerFrame": {
							"x": 726,
							"y": 318,
							"width": 82,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-row_pipeline_button_artifacts-m0rgmjux.png",
							"frame": {
								"x": 726,
								"y": 318,
								"width": 82,
								"height": 24
							}
						},
						"children": [
							{
								"objectId": "277E364E-6E96-4098-BC75-AFC906D91C13",
								"kind": "group",
								"name": "caret3",
								"originalName": "caret",
								"maskFrame": {
									"x": 0.2410714285714284,
									"y": 0,
									"width": 3.374999999999997,
									"height": 8
								},
								"layerFrame": {
									"x": 790.9999999999992,
									"y": 329,
									"width": 9,
									"height": 4
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-caret-mjc3rtm2.png",
									"frame": {
										"x": 790.9999999999992,
										"y": 329,
										"width": 9,
										"height": 4
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "79BD1ED6-C102-4B7E-8705-F80DCFB20BE7",
						"kind": "group",
						"name": "row_pipeline_button_collapse1",
						"originalName": "row_pipeline_button_collapse",
						"maskFrame": null,
						"layerFrame": {
							"x": 886,
							"y": 318,
							"width": 71,
							"height": 24
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-row_pipeline_button_collapse-nzlcrdff.png",
							"frame": {
								"x": 886,
								"y": 318,
								"width": 71,
								"height": 24
							}
						},
						"children": []
					},
					{
						"objectId": "1A1C2C32-6579-4ADD-96F0-98CA78ED86A7",
						"kind": "group",
						"name": "Column58",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 261,
							"y": 319,
							"width": 126,
							"height": 22
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-muexqzjd.png",
							"frame": {
								"x": 261,
								"y": 319,
								"width": 126,
								"height": 22
							}
						},
						"children": [
							{
								"objectId": "B0E2256F-3C93-4CC4-A7D5-7D9054EC4FCC",
								"kind": "group",
								"name": "pipelines_minipipeline_graph1",
								"originalName": "pipelines/minipipeline-graph",
								"maskFrame": null,
								"layerFrame": {
									"x": 261,
									"y": 319,
									"width": 126,
									"height": 22
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-pipelines_minipipeline_graph-qjbfmji1.png",
									"frame": {
										"x": 261,
										"y": 319,
										"width": 126,
										"height": 22
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "7BA4F602-F8AE-4F28-B21A-6F0FA2E440F5",
						"kind": "group",
						"name": "Column59",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 73,
							"y": 325,
							"width": 434,
							"height": 14
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-n0jbney2.png",
							"frame": {
								"x": 73,
								"y": 325,
								"width": 434,
								"height": 14
							}
						},
						"children": []
					},
					{
						"objectId": "9FBF2D1C-ADDA-4EB8-A2D2-B62E241F1B37",
						"kind": "group",
						"name": "Column60",
						"originalName": "Column",
						"maskFrame": null,
						"layerFrame": {
							"x": 33,
							"y": 318,
							"width": 24,
							"height": 24
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Column-ouzcrjje.png",
							"frame": {
								"x": 33,
								"y": 318,
								"width": 24,
								"height": 24
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "9852D110-501C-4FE2-B684-21E814FA26E3",
				"kind": "group",
				"name": "header1",
				"originalName": "header",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 253,
					"width": 958,
					"height": 50
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-header-otg1mkqx.png",
					"frame": {
						"x": 16,
						"y": 253,
						"width": 958,
						"height": 50
					}
				},
				"children": []
			},
			{
				"objectId": "B3B8FB5D-670B-4869-9889-A860F3404C69",
				"kind": "group",
				"name": "top1",
				"originalName": "top",
				"maskFrame": null,
				"layerFrame": {
					"x": 16,
					"y": 0,
					"width": 958,
					"height": 232
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "159F6EF7-E303-4208-8453-3644C5F080B7",
						"kind": "group",
						"name": "breadcrumbs1",
						"originalName": "breadcrumbs",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 0,
							"width": 958,
							"height": 48
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-breadcrumbs-mtu5rjzf.png",
							"frame": {
								"x": 16,
								"y": 0,
								"width": 958,
								"height": 48
							}
						},
						"children": [
							{
								"objectId": "170ECEB6-7868-48B6-A944-0955C014EA84",
								"kind": "group",
								"name": "breadcrumb_items1",
								"originalName": "breadcrumb-items",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 16,
									"width": 432,
									"height": 16
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-breadcrumb_items-mtcwrunf.png",
									"frame": {
										"x": 16,
										"y": 16,
										"width": 432,
										"height": 16
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "9FBBE3FA-582D-4435-88BD-EB6A43CF6CE1",
						"kind": "group",
						"name": "issuable_row_header1",
						"originalName": "issuable__row--header",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 63,
							"width": 958,
							"height": 48
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-issuable_row_header-ouzcqkuz.png",
							"frame": {
								"x": 16,
								"y": 63,
								"width": 958,
								"height": 48
							}
						},
						"children": [
							{
								"objectId": "34BD83C8-7AC8-4545-85BC-DAE8534AAB92",
								"kind": "group",
								"name": "meta_data1",
								"originalName": "meta-data",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 67,
									"width": 493,
									"height": 24
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-meta_data-mzrcrdgz.png",
									"frame": {
										"x": 16,
										"y": 67,
										"width": 493,
										"height": 24
									}
								},
								"children": [
									{
										"objectId": "FCF9DAEF-3B35-43C5-8574-88FCC5047E2B",
										"kind": "group",
										"name": "text1",
										"originalName": "text",
										"maskFrame": null,
										"layerFrame": {
											"x": 101,
											"y": 67,
											"width": 408,
											"height": 24
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-text-rkngourb.png",
											"frame": {
												"x": 101,
												"y": 67,
												"width": 408,
												"height": 24
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "1E9535C8-AAE8-4946-837E-447A1C6F840F",
								"kind": "group",
								"name": "actions1",
								"originalName": "actions",
								"maskFrame": null,
								"layerFrame": {
									"x": 788,
									"y": 63,
									"width": 186,
									"height": 32
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-actions-muu5ntm1.png",
									"frame": {
										"x": 788,
										"y": 63,
										"width": 186,
										"height": 32
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "9E606D35-6AA4-4680-8EE9-F8FC7932122A",
						"kind": "group",
						"name": "issuable_description1",
						"originalName": "issuable__description",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 127,
							"width": 958,
							"height": 105
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-issuable_description-ouu2mdze.png",
							"frame": {
								"x": 16,
								"y": 127,
								"width": 958,
								"height": 105
							}
						},
						"children": []
					}
				]
			}
		]
	}
]